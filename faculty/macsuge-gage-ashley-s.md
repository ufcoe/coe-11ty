---
layout: faculty-profile
name: Ashley S. MacSuga-Gage
interests:
- All Faculty
- " Assessment &amp; Evaluation"
- " At Risk &amp; Related Issues"
- " Behavior"
- " Behavior Management"
- " Class-wide Positive Behavior Supports"
- " Classroom Management"
- " Diverse Learners"
- " Educational / Instructional Design"
- " Field Experience"
- " Higher Education"
- " Intervention"
- " Mentorship"
- " Middle School / Jr. High"
- " Multi-tiered Systems of Support"
- " Online &amp; Distance Education"
- " Other School &amp; Classroom Issues"
- " Primary / Elementary"
- " Professional Development"
- " Public Education"
- " Qualitative Research"
- " Quantitative Research"
- " Research / Program Evaluation"
- " Response to Intervention"
- " School of Special Education School Psychology &amp; Early Childhood Studies"
- " School-wide Positive Behavior Supports"
- " Secondary / High School Transition"
- " Single Subject Research/Single Case Design"
- " Socialization Issues / Social Competence"
- " Technology Trends &amp; Issues"
- " Training\r                                                                               "
imageUrl: https://education.ufl.edu/faculty/files/2013/10/ASMG-Headshot-Fall-2013.jpeg
affiliations:
- Assistant Clinical Professor of Special Education &amp; Special Education Program
  Area Leader
- School of Special Education, School Psychology and Early Childhood Studies
address:
- College of Education
- University of Florida
- 1403 Norman Hall
- PO Box 117050
- Gainesville, FL 32611
- 352-294-2757
- asmg@coe.ufl.edu
degrees:
- Ph.D. in Special Education, May 2013
- University of Connecticut
- Specializing in Positive Behavior Support, Professional Development, and Measurement/Research
  Design.
- Additional Graduate Certificate:&nbsp; Positive Behavior Support (PBS)
- 'M.A. in Education: Special Education, May 2006'
- University of Connecticut
- K-12 Special Education Endorsement
- 'Research Focus: Vocabulary Intervention Targeting At-Risk Learners (Project VITAL)'
- 'B.S. in Education: Special Education, Concentration in History, May 2005'
- University of Connecticut
- 'Teacher Certification: Special Education Comprehensive K-12'
- Fulfilled requirements for an interdisciplinary concentration in History
- Graduated cum laude
appointments:
- Clinical Assistant Professor of Special Education, January 2015 &#8211; Present
- University of Florida &ndash; School of Special Education, School Psychology, and
  Early Childhood Studies (SESPECS)
- Visiting Clinical Assistant Professor of Special Education, August 2013 &ndash;
  December 2014
- University of Florida &ndash; School of Special Education, School Psychology, and
  Early Childhood Studies (SESPECS)
- Program Area Leader of Special Education, January 2014 &ndash; Present
- University of Florida &ndash; School of Special Education, School Psychology, and
  Early Childhood Studies (SESPECS)
honors:
- 'Review &amp; Editorial Activities:'
- Guest Reviewer Journal of Education for Students Placed At-Risk, 2014 &#8211; Present
- Guest Reviewer Behavioral Disorders, 2013 &ndash; Present
- Guest Reviewer Beyond Behavior, 2013 &#8211; Present
- Guest Reviewer Remedial and Special Education, 2013 &#8211; Present
- Peer Reviewer Journal of Curriculum and Instruction, 2013 &#8211; Present
- Guest Reviewer Education and Treatment of Children, 2012 &#8211; Present
- Student Reviewer Beyond Behavior, 2011 &ndash; 2013
- Council for Exceptional Children (CEC) Convention and Expo Proposal Reviewer &ndash;
  2013 &ndash; Present
- Association for Positive Behavior Support (APBS) Conference Session Proposal Reviewer
  &ndash; 2013
- 'Committee Participation:'
- Elected University of Florida College of Education's Faculty Policy Council's Alternate
  representative, May 2015 &#8211; present
- Faculty advisor for the University of Florida College of Education's Student chapter
  of the Florida Educators Association (FEA) a division of the National Education
  Association (NEA), August 2014 &ndash; present
- Ex-officio Board Member of the Council for Exceptional Children's (CEC) Division
  of the Council for Children with Behavioral Disorders (CCBD), March 2014-present
- Co-Chair of the Council for Exceptional Children's (CEC) Division of the Council
  for Children with Behavioral Disorders (CCBD) Professional Development Committee
  (PDC), March 2014-present
- Member Association for Positive Behavior Support (APBS) Training and Education Committee,
  March 2012-present
- Member Diversity Committee College of Education at University of Florida, August
  2013-May 2015
- Member Florida Association for Positive Behavior Support (FLAPBS) Network and Membership
  Committee, June 2013 &#8211; present
- Leader Association for Positive Behavior Support (APBS) Student Workgroup, March
  2013- June 2014
- President Association for Positive Behavior Support (APBS) Student Network &amp;
  Ex-officio Board Member APBS, March 2012 &ndash; March 2013
- Member Association for Positive Behavior Support (APBS) Membership Committee, Fall
  2012-present
- Member Association for Positive Behavior Support (APBS) Student Network, 2011-2013
- 'Awards/Scholarships:'
- Anderson Scholar Faculty Honoree, University of Florida, College of Liberal Arts
  and Sciences (2014)
- Recipient of the Midwest Symposium for Leadership in Behavior Disorders (MSLBD)
  Doctoral Stipend Award (2013, February)
- Neag Graduate Student Outstanding Research Travel Award (2012, Fall)
- Recipient of the Lisa Pappanikou Glidden Scholarship (2012, April)
- Neag Graduate Student Association Travel Award (2012, Spring)
- Neag Graduate Student Association Travel Award (2011, Spring)
grants:
- 'Institute of Educational Sciences (IES) Grant Program, "Project PARTICL: Psychometric
  evaluation of an Audio Recording-based Instrument to Assess Teacher''s Implementation
  of Evidence-based Classroom Management Skills," PI''s Nicholas A. Gage &amp; Ashley
  S. MacSuga-Gage &#8211; $1.4 million &ndash; Under Review (2015)'
- 'College Research Initiative Fund (CRIF) Grant Program, "Project MTSS-PD: A Data-based
  Decision-Making Professional Development Package to Increase Teachers'' Use of Evidence-based
  Classroom Management Practices," PI''s Nicholas A. Gage &amp; Ashley S. MacSuga-Gage
  &#8211; $5,000 &ndash; Funded (2015)'
- University of South Florida Florida's Positive Behavior Support Project Grant, "Florida
  Positive Behavior Support Project," PI Subcontract Ashley S. MacSuga-Gage &#8211;
  $50,000 per year funded &#8211; Funded (Spring 2015 &ndash; present)
- Florida Department of Education Teacher Quality Partnership Grant Program, "Advancing
  the Development of Pre-service Teachers Project ADePT," PI's Buffy Bondy &amp; Ester
  De Jong &ndash; (worked with grant writing team on conceptualization and Evaluation)-
  Funded (2014)
- 'University of Florida Opportunity Grant, "Project ENGAGE: Evaluating the Relationship
  Between Classroom Management and Student Engagement", PI''s Nicholas A. Gage, Ashley
  S. MacSuga-Gage, &amp; Timothy Vollmer &ndash; Unfunded (2013)'
- The Wing Institute Graduate Research Funding Program in Evidence-based Education
  Grant &#8211; $5,000.00 &ndash; Funded (2012)
publications:
  'Articles in Peer Refereed Journals:':
  - 'Schmidt, M., MacSuga-Gage, A., Gage, N., Cox., P., &amp; McLeskey, J., (2015).
    Bringing the field to the supervisor: Innovation in distance supervision for field-based
    experiences using mobile technologies. Rural Special Education Quarterly, 34(1).
    37-43.'
  - 'MacSuga-Gage, A.S., &amp; Gage, N.A. (2015) Student-level effects of increased
    teacher-directed opportunities to respond. Journal of Behavioral Education. Advance
    online publication. doi: 10.1007/s10864-015-9223-2.'
  - MacSuga-Gage, A. S., McNiff, M., Schmidt, M., Gage, N. A. &amp; Schmidt, C. (in
    press). &nbsp;Is there an app for that? A model to help school-based professionals
    identify, implement, and evaluate technology for problem behaviors. Beyond Behavior.
  - Gage, N.A., Wilson, J., &amp; MacSuga-Gage, A.S. (2015). Writing performance of
    students with behavioral disabilities. Behavioral Disorders.
  - 'Gage, N. A., &amp; MacSuga-Gage, A.S. (2015). Students with limited English proficiency
    and emotional and/or behavioral disorders: Prevalence, characteristics, and future
    directions. Beyond Behavior.'
  - 'MacSuga-Gage, A. S., &amp; Simonsen, B. (in press). Examining the effects of
    teacher-directed opportunities to respond on student outcomes: A systematic review
    of the literature. Education and Treatment of Children.'
  - 'Freeman, J., Simonsen, B., Briere, D. E., &amp; MacSuga-Gage, A. S. (2014). Teacher
    training in classroom management: A review of state accreditation policy and teacher
    preparation programs. Teacher Education &amp; Special Education. 37, 106-120.'
  - 'Simonsen, B., MacSuga-Gage, A. S., Briere, D. E., Freeman, J., Myers, D., Scott,
    T., &amp; Sugai, G. (2014). &nbsp;Multi-Tiered Support Framework for Teachers''
    Classroom Management Practices: Overview and Case Study of Building the Triangle
    for Teachers. Journal of Positive Behavior Interventions, 16(3), 179-190. doi:
    10.1177/1098300713484062.'
  - 'Haydon, T., MacSuga-Gage, A. S., Simonsen, B., &amp; Hawkins, R. (2012). Opportunities
    to respond: A key component of effective instruction. Beyond Behavior, 22, 23-31.'
  - MacSuga-Gage, A. S., Simonsen, B., &amp; Briere, D. E. (2012). Effective teaching
    practices that promote a positive classroom environment. Beyond Behavior, 22,
    14-22.
  - 'Simonsen, B., MacSuga, A. S., Fallon, L. M., &amp; Sugai, G. (2012). Teacher
    self-monitoring to increase specific praise rates. Journal of Positive Behavior
    Interventions. Advance online publication. doi: 10.1177/1098300712440453'
  - 'MacSuga, A. S., &amp; Simonsen, B. (2011). Increasing teachers'' use of evidence-based
    classroom management strategies through consultation: Overview and case studies.
    Beyond Behavior, 20(1), 4-12.'
  'Book Chapters:':
  - 'Jolivette, K., MacSuga-Gage, A.S., &amp; Evanovich, L. (in review). Students
    with emotional and behavioral disorders. Y. Bui &amp; E. Meyen (Eds.), Exceptional
    children in today''s schools: What teachers need to know (4th ed.). Love Publishing
    Company.'
  - 'MacSuga-Gage, A. S. (2013). Engaging students through opportunities to respond.
    In Classroom Management: An A to Z Guide. To be retrieved from SAGE Publications
    online.'
  - 'MacSuga-Gage, A. S. (2013). Application of&nbsp;positive behavioral interventions
    and supports&nbsp;to school-wide and classroom settings. In Classroom Management:
    An A to Z Guide. To be retrieved from SAGE Publications online.'
links:
- Florida's Positive Behavioral Intervention and Support (FLPBIS) Project http://flpbs.fmhi.usf.edu
- Association for Positive Behavior Support http://apbs.org
- Council for Children with Behavioral Disorders http://www.ccbd.net/CCBD/Home/
- National Technical Assistance Center for Positive Behavior Interventions and Supports
  http://pbis.org
- Dr. MacSuga-Gage&#8217;s Curriculum Vitae

---
Dr. MacSuga-Gage is a Clinical Assistant Professor of Special Education at the University of Florida and serves as the Special Education Program Area leader for the college's department of Special Education, School Psychology, and Early Childhood Studies (SESPECS). She has a Ph.D. in Special Education from the University of Connecticut where she studied Positive Behavior Support (PBS), School-Wide Positive Behavior Interventions and Supports (SWPBIS), and Applied Behavioral Analysis (ABA). In addition to her doctoral studies at the University of Connecticut, she earned a graduate certificate in Positive Behavior Interventions and Supports (PBIS). Her specific research interests include identifying and supporting teachers in the implementation of Class-Wide Positive Behavior Support (CWPBS) practices through the application of Multi-Tiered Systems of Support (MTSS) to professional development and the implementation, sustainability, and scale-up of SWPBIS efforts. Other research interests include delivery of instruction, coaching, and mentoring via web-based formats (eSupervision) to support professional development for pre-service and in-service teachers and, supporting the needs of students at-risk for and identified with Emotional and Behavioral Disorders (EBD). In addition to her work at the University of Florida, she is engaged with the University of South Florida's Positive Behavioral Interventions and Supports (FLPBIS) Project (http:\/\/flpbs.fmhi.usf.edu). Aside from her continued work surrounding behavioral support for students, families and practitioners, Dr. MacSuga-Gage is currently working with Dr. James McLeskey and Dr. Penny Cox on Project Restructuring and Improving Teacher Education (RITE). This project focuses on leveraging and infusing technology throughout the graduate pre-service teaching experience of future special educators at the University of Florida.