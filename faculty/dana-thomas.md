---
layout: faculty-profile
name: Thomas Dana
jobTitle: Senior Associate Dean for Academic Affairs
imageUrl: https://s3.amazonaws.com/coe-forestry/Tom-Dana.jpeg
phone: "(352) 273-4134"
email: tdana@coe.ufl.edu
twitter: ''
address:
- University of Florida
- College of Education
- 140 C Norman Hall
- PO Box 117040
- Gainesville, FL 32611
degrees:
- Ph.D., Florida State University
- M.S., State University of New York at Oswego
- B.S., State University of New York at Oswego
affiliations: []
interests: []
activities:
- Associate Professor, Penn State University, 1997-2003
- Assistant Professor, Penn State University, 1992-1997
grants: []
publications:
  Books: []
  Articles: []

---
Tom Dana is Professor of Education and the Associate Dean for Academic Affairs in the College of Education at the University of Florida. Prior to joining the dean’s office in 2008, he held the post of director (chair) of the School of Teaching and Learning for five years. He led the college’s effort in creating models and support systems to launch and grow distance learning programs. Closely working with UF’s Lastinger Center for Learning, he was instrumental in creating job-embedded graduate programs for educators in some of the most challenging school settings in Florida. Dana is the co-director, with Alan Dorsey from the College of Liberal Arts and Science, of UFTeach – an innovative model to increase the quality and quantity of science and mathematics teachers. In 2010, he led the college through a successful accreditation review by the Florida Department of Education and the National Council for the Accreditation of Teacher Education. Dana’s academic interests are in science education, particularly the study of quality educator professional development programs and the development of reform-oriented, subject-specific pedagogical knowledge and practice.

Prior to his appointment at UF, Dr. Dana was a member of the faculty at The Pennsylvania State University since 1992. While at Penn State, he served as Coordinator of Teacher Education and held the Henry J. Hermanowicz Professorship in Teacher Education. Dana earned a B.S. in Earth Sciences Education and a M.S. in Science Education from the State University of New York at Oswego, and a Ph.D. in Science Education from The Florida State University. He holds New York State teacher certification in earth science, physics, and general science and taught middle and high school earth science and physical science in Mexico, NY, and physics and physical science in Tallahassee, FL.