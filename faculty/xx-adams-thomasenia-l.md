---
layout: faculty-profile
name: Thomasenia Adams
jobTitle: Professor and Associate Dean for Research and Faculty
imageUrl: https://s3.amazonaws.com/coe-forestry-test2/Thomasenia-Adams-1.jpeg
phone: "(352) 273-4116"
email: tla@coe.ufl.edu
twitter: TLAMath
address:
- 2-230G Norman Hall
- PO Box 117040
- Gainesville FL, 32611
degrees:
- Doctor of Philosophy, Curriculum & Instruction, University of Florida, Gainesville,
  FL, 1993
- Master of Education, Curriculum & Instruction, University of Florida, Gainesville,
  FL, 1990
- Bachelor of Science, Mathematics, South Carolina State College, Orangeburg, SC,
  1987
affiliations:
- Office of Educational Research
- Lastinger Center for Learning
- School of Teaching and Learning
interests:
- Diverse Learners
- Mathematics Education
- Middle School/Jr. High
- Primary/Elementary
- Professional Development
- Secondary/High School Transition
activities:
- William R. Jones Outstanding Mentor Award, Florida Education Fund (McKnight Fellowship
  Program), 2015
- Top 25 Education Professors in Florida, 2013
- ESPNU Success Stories, 2012
grants:
- title: An Investigation of Middle and High School African American Female Students’
    Positionality in Science and Development of the Postionality Index Instrument
  grantor: National Science Foundation
  role: CoPI
  awardamount: "$495,524"
- title: 'STEM EduGators: UF Noyce Scholars Program'
  grantor: National Science Foundation
  role: Co-PI
  awardamount: "$1,215,872"
  awarddate: '2012'
- title: The Florida STEM-Teacher Induction and Professional Support Center
  grantor: Florida Department of Education
  role: Co-PI
  awardamount: "$1,860,639.03"
  awarddate: 7/1/2012–6/30/2014
publications:
  Books:
  - title: Estimation at Work
    authors: Thomasenia Lott Adams, Gregory Harrell
    pubdate: Learning and Teaching Measurement
    booktitle: National Council of Teachers of Mathematics
    bookauthors: "2003"
    pubcity: Reston
    publisher: NCTM
    pages: 229-244
  - title: Maximizing Manipulatives
    authors: Thomasenia Lott Adams
    pubdate: 'Empowering the Beginning Teacher of Mathematics: Elementary School'
    booktitle: M. Chappell (Ed.)
    bookauthors: "2004"
    pubcity: Reston
    publisher: NCTM
    pages: "2"
  - title: Maximizing Manipulatives
    authors: Thomasenia Lott Adams
    pubdate: 'Empowering the Beginning Teacher of Mathematics: Middle School'
    booktitle: M. Chappell (Ed.)
    bookauthors: "2004"
    pubcity: Reston
    publisher: NCTM
    pages: "1"
  - title: Meeting the challenge of engaging students for success in mathematics by
      using culturally responsive
    authors: Peterek, E. & Adams, T. L.
    pubdate: Responding to Diversity, Grades PreK-5
    booktitle: White, D. Y & Spitzer, J. S.
    bookauthors: "2009"
    pubcity: Reston, VA
    publisher: National Council of Teachers of Mathematics
    pages: "12"
  - title: 'Focus in Grade 5: Teaching With Curriculum Focal  Points'
    authors: Bekmann, S., Fuson, K. C., SanGiovanni, J. & Adams, T. L.
    pubdate: in press
    pubcity: Reston, VA
    publisher: National Council of Teachers of Mathematics
  - title: Focus in grade 5; Teaching with curriculum focal points
    authors: Fuson, K. C, SanGiovanni, J., & Adams, T. L.
    pubdate: "2009"
    pubcity: Reston, VA
    publisher: National Council of Teachers of Mathematics
  - title: 'Common core mathematics in a PLC at work: Grades PreK-2'
    authors: Larson, M., Fennell, F., Adams, T. L., Dixon, J., Kobett, B., & Wray,
      J.
    pubdate: "2012"
    pubcity: Bloomington, IN
    publisher: Solutions Tree & National Council of Teachers of Mathematics
  - title: 'Common core mathematics in a PLC at work: Grades 3-5'
    authors: Larson, M., Fennell, F., Adams, T. L., Dixon, J., Kobett, B., & Wray,
      J.
    pubdate: "2012"
    pubcity: Bloomington, IN
    publisher: Solutions Tree & National Council of Teachers of Mathematics
  Articles:
  - title: 'Discovery and Transformation:  Reflections on Teaching a New Mathematics
      Education Course'
    authors: Thomasenia Lott Adams
    journal: 'Teaching & Learning:  The Journal of Natural Inquiry'
    jnum: 15(1)
    pubdate: "2001"
    pages: 36-43
  - title: Making  Connections With the Vedic Square
    authors: Thomasenia Lott Adams
    journal: ENC Focus
    jnum: "9"
    pubdate: "2002"
    pages: 22-25
  - title: 'Reading Mathematics: More Than Words Can Say'
    authors: Thomasenia Lott Adams
    journal: The Reading Teacher
    jnum: "56"
    pubdate: "2003"
    pages: 786-795
  - title: 'Teachers Speak: Promoting Curriculum Integration With Success'
    authors: Thomasenia Lott Adams & Delores C. S. James
    journal: Florida Educational Leadership
    jnum: "4"
    pubdate: "2003"
    pages: 44-48
  - title: 'A Primer for Pre-problem Ponderings: Anticipating the Answer'
    authors: Karen S. Cohen & Thomasenia Lott Adams
    journal: Mathematics Teacher
    jnum: "97"
    pubdate: "2004"
    pages: 110-115
  - title: 'Providing Links Between Technology Integration, Methods Courses and Traditional
      Field Experiences: Implementing a Model of Curriculum-Based and Technology-Enhanced
      Microteaching'
    authors: Kara Dawson, Rose Pringle & Thomasenia Lott Adams
    journal: Journal of Computing in Teacher Education
    jnum: "20"
    pubdate: "2003"
    pages: 41-47
  - title: 'Technology, Science and Pre-service Teachers: Creating a Culture of Technology-Savvy
      Elementary Teachers'
    authors: Rose M. Pringle, Kara Dawson & Thomasenia Lott Adams
    journal: Action in Teacher Education
    jnum: "24"
    pubdate: "2003"
    pages: 46-52
  - title: Weigh to go! Multiple-meaning, same-sounding, & like-sounding words in
      mathematics
    authors: Thomasenia Lott Adams, Fiona Thangata, & Cindy King
    journal: Mathematics Teaching in the Middle School
    jnum: In press
    pubdate: ''
    pages: ''
  - title: Weigh to go! Exploring mathematical language
    authors: Adams, Thangata, King
    journal: Mathematics Teaching in the Middle School
    jnum: "10"
    pubdate: "2005"
    pages: 444-448
  - title: 'Reading mathematics: An introduction'
    authors: Adams, T. L.
    journal: Reading & Writing Quarterly
    jnum: "23"
    pubdate: "2007"
    pages: 117-119
  - title: An analysis of children&#39;s strategies for reading mathematics
    authors: Adams, T. L. & Lowery, R. M.
    journal: Reading & Writing Quarterly
    jnum: "23"
    pubdate: "2007"
    pages: 161-177
  - title: "2008"
    authors: West-Olatunji, Pringle, Adams, Baratelli, Goodman & Maxis
    journal: International Journal on Learning
    jnum: 14(9)
    pubdate: "2008"
    pages: 219-227
  - title: Rite of Passage programs as strength-based interventions to increase mathematics
      and science achievement among low-income, African American youth
    authors: West-Olatunji, C., Shure, L, Pringle, R., Adams, T. L., Baratelli, A.,
      Milton, K., Flesner, D. M., & Lewis, D.
    journal: International Journal of Learning
    jnum: ''
    pubdate: Accepted
    pages: ''
  - title: A Venn diagram model for professionald development for teachers of mathematics
    authors: Adams, T. L. & Aslan-Tutak, F.
    journal: Southeastern Regional Association of Teacher Educators Journal
    jnum: "17"
    pubdate: "2008"
    pages: 19-24
  - title: 'Multicultural Mathematics: Revelations from an Exploratory Assignment'
    authors: Adams, T. L.
    journal: Multicultural Education
    jnum: "15"
    pubdate: "2008"
    pages: 51-55
  - title: Rite of passage programs as strength-based interventions to increase mathematics
      and science achievement among low-income, African American youth
    authors: West-Olatunji, C., Shure, L.*, Pringle, R., Adams, T. L., Baratelli,
      A.*, Milton, K.*, Felsner, D. M., & Lewis, D.*  (*Graduate Student)
    journal: The International Journal of Learning
    jnum: ''
    pubdate: In Press
    pages: ''
  - title: Exploring how school counselors position low-income African American girls
      as mathematics and science learners
    authors: West-Olatunji, C., Shure, L, Pringle, R., Adams, T., Lewis, D., & Cholewa,
      B.
    journal: Professional School Counseling
    jnum: "13"
    pubdate: "2010"
    pages: 184-195
  - title: Factors influencing elementary teachers&#39; positioning of African American
      girls as science and mathematics learners
    authors: Pringle, R. M., Milton, K., Adams, T., West-Olatunji, C., & Archer-Banks,
      D.
    journal: School Science and Mathematics
    jnum: ''
    pubdate: Submitted
    pages: ''
  - title: A study of estimation in the culture of professionals at work
    authors: Adams, T. L. & Harrell, G.
    journal: Journal of Mathematics & Culture
    jnum: ''
    pubdate: Accepted
    pages: ''
  - title: Factors influencing elementary teachers&#39; positioning of African American
      girls as sceince and mathematics learners
    authors: Pringle, R. M., Milton, K., Adams, T., West-Oltanji, C., & Archer-Banks,
      D.
    journal: School Science and Mathematics
    jnum: ''
    pubdate: In Press
    pages: ''
  - title: A study of estimation in the culture of professionals at work
    authors: Adams, T. L. & Harrell, G.
    journal: Journal of Mathematics and Culture
    jnum: "5"
    pubdate: "2010"
    pages: 1-15
  - title: Critical pedagogy for critical mathematics education
    authors: Aslan-Tutak, F., Bondy, E., & Adams, T. L.
    journal: International Journal of Mathematical Education in Science and Technolgy
    jnum: "42"
    pubdate: "2010"
    pages: 65-74
  - title: Exploring how school counselors position low-income African American girls
      as mathematics and science learners
    authors: West-Olatunji, C., Shure, L., Pringle, R., Adams, T., Lewis, D., & Cholewa,
      B.
    journal: Professional School Counseling
    jnum: "13"
    pubdate: "2010"
    pages: 184-195
  - title: 'Culturally responsive teaching in the context of mathematics: A grounded
      theory case study'
    authors: Bonner, E. & Adams, T. L.
    journal: Journal for Mathematics Teacher Education
    jnum: ''
    pubdate: Revised and resubmitted
    pages: 25 pages
  - title: How African American parents position their daughters as mathematics and
      science learners
    authors: West-Olatunji, C., Lewis, D., Pringle, R., Adams, T., Byrd, K., LaFramenta,
      J., Cholewa, B., Sanders, T., & Baratelli, A.
    journal: Education and Urban Society
    jnum: ''
    pubdate: Submitted February 2010
    pages: 24 pages
  - title: 'Culturally responsive teaching in the context of mathematics: A grounded
      theory case study'
    authors: Bonner, E. & Adams, T. L.
    journal: Journal for Mathematics Teacher Education
    jnum: "15"
    pubdate: "2012"
    pages: 25-38

---
I am a professor of mathematics education in the School of Teaching and Learning in the College of Education at the University of Florida. I also serve as the college’s Associate Dean of Research and Faculty Development.

In the role of faculty, I have a commendable list of publications, presentations, and professional activities. Some notable recent accomplishments include the following:

* UF 2018 Doctoral Commencement Speaker
* Co-author of Making Sense of Mathematics for Teaching series (Solution Tree)
* Program Chair for the 2018 National Council of Teachers of Mathematics’ Annual Conference
* Associate editor of Mathematics Teacher: Learning and Teaching PreK-12 (NCTM)
* Mathematics Program Officer for the UF Lastinger Center for Learning

In the role of associate dean, I have the privilege of leading the Office of Educational Research and providing
service to the faculty to support external funding efforts to further research. My leadership has led to many
highlights including consistent annual increases in the number of proposals submitted, the number of proposals
funded, the amount of new funding, and amount of currently funded projects.