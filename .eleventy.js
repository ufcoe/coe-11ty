module.exports = function(eleventyConfig) {
    eleventyConfig.addPassthroughCopy('components.js');
    eleventyConfig.addPassthroughCopy('css');
    return {
      dir: {
        // ⚠️ These values are both relative to your input directory.
        includes: "_includes",
        layouts: "_layouts"
      }
    }
  };