/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */
const t="undefined"!=typeof window&&null!=window.customElements&&void 0!==window.customElements.polyfillWrapFlushCallback,e=(t,e,i=null)=>{for(;e!==i;){const i=e.nextSibling;t.removeChild(e),e=i}},i=`{{lit-${String(Math.random()).slice(2)}}}`,o=`\x3c!--${i}--\x3e`,n=new RegExp(`${i}|${o}`);class s{constructor(t,e){this.parts=[],this.element=e;const o=[],s=[],a=document.createTreeWalker(e.content,133,null,!1);let d=0,h=-1,p=0;const{strings:g,values:{length:u}}=t;for(;p<u;){const t=a.nextNode();if(null!==t){if(h++,1===t.nodeType){if(t.hasAttributes()){const e=t.attributes,{length:i}=e;let o=0;for(let t=0;t<i;t++)r(e[t].name,"$lit$")&&o++;for(;o-- >0;){const e=g[p],i=l.exec(e)[2],o=i.toLowerCase()+"$lit$",s=t.getAttribute(o);t.removeAttribute(o);const r=s.split(n);this.parts.push({type:"attribute",index:h,name:i,strings:r}),p+=r.length-1}}"TEMPLATE"===t.tagName&&(s.push(t),a.currentNode=t.content)}else if(3===t.nodeType){const e=t.data;if(e.indexOf(i)>=0){const i=t.parentNode,s=e.split(n),a=s.length-1;for(let e=0;e<a;e++){let o,n=s[e];if(""===n)o=c();else{const t=l.exec(n);null!==t&&r(t[2],"$lit$")&&(n=n.slice(0,t.index)+t[1]+t[2].slice(0,-"$lit$".length)+t[3]),o=document.createTextNode(n)}i.insertBefore(o,t),this.parts.push({type:"node",index:++h})}""===s[a]?(i.insertBefore(c(),t),o.push(t)):t.data=s[a],p+=a}}else if(8===t.nodeType)if(t.data===i){const e=t.parentNode;null!==t.previousSibling&&h!==d||(h++,e.insertBefore(c(),t)),d=h,this.parts.push({type:"node",index:h}),null===t.nextSibling?t.data="":(o.push(t),h--),p++}else{let e=-1;for(;-1!==(e=t.data.indexOf(i,e+1));)this.parts.push({type:"node",index:-1}),p++}}else a.currentNode=s.pop()}for(const t of o)t.parentNode.removeChild(t)}}const r=(t,e)=>{const i=t.length-e.length;return i>=0&&t.slice(i)===e},a=t=>-1!==t.index,c=()=>document.createComment(""),l=/([ \x09\x0a\x0c\x0d])([^\0-\x1F\x7F-\x9F "'>=/]+)([ \x09\x0a\x0c\x0d]*=[ \x09\x0a\x0c\x0d]*(?:[^ \x09\x0a\x0c\x0d"'`<>=]*|"[^"]*|'[^']*))$/;function d(t,e){const{element:{content:i},parts:o}=t,n=document.createTreeWalker(i,133,null,!1);let s=p(o),r=o[s],a=-1,c=0;const l=[];let d=null;for(;n.nextNode();){a++;const t=n.currentNode;for(t.previousSibling===d&&(d=null),e.has(t)&&(l.push(t),null===d&&(d=t)),null!==d&&c++;void 0!==r&&r.index===a;)r.index=null!==d?-1:r.index-c,s=p(o,s),r=o[s]}l.forEach(t=>t.parentNode.removeChild(t))}const h=t=>{let e=11===t.nodeType?0:1;const i=document.createTreeWalker(t,133,null,!1);for(;i.nextNode();)e++;return e},p=(t,e=-1)=>{for(let i=e+1;i<t.length;i++){const e=t[i];if(a(e))return i}return-1};
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */
const g=new WeakMap,u=t=>"function"==typeof t&&g.has(t),m={},x={};
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */
class f{constructor(t,e,i){this.t=[],this.template=t,this.processor=e,this.options=i}update(t){let e=0;for(const i of this.t)void 0!==i&&i.setValue(t[e]),e++;for(const t of this.t)void 0!==t&&t.commit()}_clone(){const e=t?this.template.element.content.cloneNode(!0):document.importNode(this.template.element.content,!0),i=[],o=this.template.parts,n=document.createTreeWalker(e,133,null,!1);let s,r=0,c=0,l=n.nextNode();for(;r<o.length;)if(s=o[r],a(s)){for(;c<s.index;)c++,"TEMPLATE"===l.nodeName&&(i.push(l),n.currentNode=l.content),null===(l=n.nextNode())&&(n.currentNode=i.pop(),l=n.nextNode());if("node"===s.type){const t=this.processor.handleTextExpression(this.options);t.insertAfterNode(l.previousSibling),this.t.push(t)}else this.t.push(...this.processor.handleAttributeExpressions(l,s.name,s.strings,this.options));r++}else this.t.push(void 0),r++;return t&&(document.adoptNode(e),customElements.upgrade(e)),e}}
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */const v=` ${i} `;class w{constructor(t,e,i,o){this.strings=t,this.values=e,this.type=i,this.processor=o}getHTML(){const t=this.strings.length-1;let e="",n=!1;for(let s=0;s<t;s++){const t=this.strings[s],r=t.lastIndexOf("\x3c!--");n=(r>-1||n)&&-1===t.indexOf("--\x3e",r+1);const a=l.exec(t);e+=null===a?t+(n?v:o):t.substr(0,a.index)+a[1]+a[2]+"$lit$"+a[3]+i}return e+=this.strings[t],e}getTemplateElement(){const t=document.createElement("template");return t.innerHTML=this.getHTML(),t}}
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */const b=t=>null===t||!("object"==typeof t||"function"==typeof t),y=t=>Array.isArray(t)||!(!t||!t[Symbol.iterator]);class k{constructor(t,e,i){this.dirty=!0,this.element=t,this.name=e,this.strings=i,this.parts=[];for(let t=0;t<i.length-1;t++)this.parts[t]=this._createPart()}_createPart(){return new S(this)}_getValue(){const t=this.strings,e=t.length-1;let i="";for(let o=0;o<e;o++){i+=t[o];const e=this.parts[o];if(void 0!==e){const t=e.value;if(b(t)||!y(t))i+="string"==typeof t?t:String(t);else for(const e of t)i+="string"==typeof e?e:String(e)}}return i+=t[e],i}commit(){this.dirty&&(this.dirty=!1,this.element.setAttribute(this.name,this._getValue()))}}class S{constructor(t){this.value=void 0,this.committer=t}setValue(t){t===m||b(t)&&t===this.value||(this.value=t,u(t)||(this.committer.dirty=!0))}commit(){for(;u(this.value);){const t=this.value;this.value=m,t(this)}this.value!==m&&this.committer.commit()}}class ${constructor(t){this.value=void 0,this.i=void 0,this.options=t}appendInto(t){this.startNode=t.appendChild(c()),this.endNode=t.appendChild(c())}insertAfterNode(t){this.startNode=t,this.endNode=t.nextSibling}appendIntoPart(t){t.o(this.startNode=c()),t.o(this.endNode=c())}insertAfterPart(t){t.o(this.startNode=c()),this.endNode=t.endNode,t.endNode=this.startNode}setValue(t){this.i=t}commit(){if(null===this.startNode.parentNode)return;for(;u(this.i);){const t=this.i;this.i=m,t(this)}const t=this.i;t!==m&&(b(t)?t!==this.value&&this.s(t):t instanceof w?this.l(t):t instanceof Node?this.h(t):y(t)?this.p(t):t===x?(this.value=x,this.clear()):this.s(t))}o(t){this.endNode.parentNode.insertBefore(t,this.endNode)}h(t){this.value!==t&&(this.clear(),this.o(t),this.value=t)}s(t){const e=this.startNode.nextSibling,i="string"==typeof(t=null==t?"":t)?t:String(t);e===this.endNode.previousSibling&&3===e.nodeType?e.data=i:this.h(document.createTextNode(i)),this.value=t}l(t){const e=this.options.templateFactory(t);if(this.value instanceof f&&this.value.template===e)this.value.update(t.values);else{const i=new f(e,t.processor,this.options),o=i._clone();i.update(t.values),this.h(o),this.value=i}}p(t){Array.isArray(this.value)||(this.value=[],this.clear());const e=this.value;let i,o=0;for(const n of t)i=e[o],void 0===i&&(i=new $(this.options),e.push(i),0===o?i.appendIntoPart(this):i.insertAfterPart(e[o-1])),i.setValue(n),i.commit(),o++;o<e.length&&(e.length=o,this.clear(i&&i.endNode))}clear(t=this.startNode){e(this.startNode.parentNode,t.nextSibling,this.endNode)}}class C{constructor(t,e,i){if(this.value=void 0,this.i=void 0,2!==i.length||""!==i[0]||""!==i[1])throw new Error("Boolean attributes can only contain a single expression");this.element=t,this.name=e,this.strings=i}setValue(t){this.i=t}commit(){for(;u(this.i);){const t=this.i;this.i=m,t(this)}if(this.i===m)return;const t=!!this.i;this.value!==t&&(t?this.element.setAttribute(this.name,""):this.element.removeAttribute(this.name),this.value=t),this.i=m}}class E extends k{constructor(t,e,i){super(t,e,i),this.single=2===i.length&&""===i[0]&&""===i[1]}_createPart(){return new z(this)}_getValue(){return this.single?this.parts[0].value:super._getValue()}commit(){this.dirty&&(this.dirty=!1,this.element[this.name]=this._getValue())}}class z extends S{}let O=!1;(()=>{try{const t={get capture(){return O=!0,!1}};window.addEventListener("test",t,t),window.removeEventListener("test",t,t)}catch(t){}})();class A{constructor(t,e,i){this.value=void 0,this.i=void 0,this.element=t,this.eventName=e,this.eventContext=i,this.g=t=>this.handleEvent(t)}setValue(t){this.i=t}commit(){for(;u(this.i);){const t=this.i;this.i=m,t(this)}if(this.i===m)return;const t=this.i,e=this.value,i=null==t||null!=e&&(t.capture!==e.capture||t.once!==e.once||t.passive!==e.passive),o=null!=t&&(null==e||i);i&&this.element.removeEventListener(this.eventName,this.g,this.u),o&&(this.u=T(t),this.element.addEventListener(this.eventName,this.g,this.u)),this.value=t,this.i=m}handleEvent(t){"function"==typeof this.value?this.value.call(this.eventContext||this.element,t):this.value.handleEvent(t)}}const T=t=>t&&(O?{capture:t.capture,passive:t.passive,once:t.once}:t.capture)
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */;function _(t){let e=j.get(t.type);void 0===e&&(e={stringsArray:new WeakMap,keyString:new Map},j.set(t.type,e));let o=e.stringsArray.get(t.strings);if(void 0!==o)return o;const n=t.strings.join(i);return o=e.keyString.get(n),void 0===o&&(o=new s(t,t.getTemplateElement()),e.keyString.set(n,o)),e.stringsArray.set(t.strings,o),o}const j=new Map,U=new WeakMap;
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */const P=new
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */
class{handleAttributeExpressions(t,e,i,o){const n=e[0];if("."===n){return new E(t,e.slice(1),i).parts}return"@"===n?[new A(t,e.slice(1),o.eventContext)]:"?"===n?[new C(t,e.slice(1),i)]:new k(t,e,i).parts}handleTextExpression(t){return new $(t)}};
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */"undefined"!=typeof window&&(window.litHtmlVersions||(window.litHtmlVersions=[])).push("1.2.1");const F=(t,...e)=>new w(t,e,"html",P)
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */,M=(t,e)=>`${t}--${e}`;let N=!0;void 0===window.ShadyCSS?N=!1:void 0===window.ShadyCSS.prepareTemplateDom&&(console.warn("Incompatible ShadyCSS version detected. Please update to at least @webcomponents/webcomponentsjs@2.0.2 and @webcomponents/shadycss@1.3.1."),N=!1);const L=t=>e=>{const o=M(e.type,t);let n=j.get(o);void 0===n&&(n={stringsArray:new WeakMap,keyString:new Map},j.set(o,n));let r=n.stringsArray.get(e.strings);if(void 0!==r)return r;const a=e.strings.join(i);if(r=n.keyString.get(a),void 0===r){const i=e.getTemplateElement();N&&window.ShadyCSS.prepareTemplateDom(i,t),r=new s(e,i),n.keyString.set(a,r)}return n.stringsArray.set(e.strings,r),r},I=["html","svg"],R=new Set,D=(t,e,i)=>{R.add(t);const o=i?i.element:document.createElement("template"),n=e.querySelectorAll("style"),{length:s}=n;if(0===s)return void window.ShadyCSS.prepareTemplateStyles(o,t);const r=document.createElement("style");for(let t=0;t<s;t++){const e=n[t];e.parentNode.removeChild(e),r.textContent+=e.textContent}(t=>{I.forEach(e=>{const i=j.get(M(e,t));void 0!==i&&i.keyString.forEach(t=>{const{element:{content:e}}=t,i=new Set;Array.from(e.querySelectorAll("style")).forEach(t=>{i.add(t)}),d(t,i)})})})(t);const a=o.content;i?function(t,e,i=null){const{element:{content:o},parts:n}=t;if(null==i)return void o.appendChild(e);const s=document.createTreeWalker(o,133,null,!1);let r=p(n),a=0,c=-1;for(;s.nextNode();){for(c++,s.currentNode===i&&(a=h(e),i.parentNode.insertBefore(e,i));-1!==r&&n[r].index===c;){if(a>0){for(;-1!==r;)n[r].index+=a,r=p(n,r);return}r=p(n,r)}}}(i,r,a.firstChild):a.insertBefore(r,a.firstChild),window.ShadyCSS.prepareTemplateStyles(o,t);const c=a.querySelector("style");if(window.ShadyCSS.nativeShadow&&null!==c)e.insertBefore(c.cloneNode(!0),e.firstChild);else if(i){a.insertBefore(r,a.firstChild);const t=new Set;t.add(r),d(i,t)}};window.JSCompiler_renameProperty=(t,e)=>t;const H={toAttribute(t,e){switch(e){case Boolean:return t?"":null;case Object:case Array:return null==t?t:JSON.stringify(t)}return t},fromAttribute(t,e){switch(e){case Boolean:return null!==t;case Number:return null===t?null:Number(t);case Object:case Array:return JSON.parse(t)}return t}},V=(t,e)=>e!==t&&(e==e||t==t),B={attribute:!0,type:String,converter:H,reflect:!1,hasChanged:V};class G extends HTMLElement{constructor(){super(),this._updateState=0,this._instanceProperties=void 0,this._updatePromise=new Promise(t=>this._enableUpdatingResolver=t),this._changedProperties=new Map,this._reflectingProperties=void 0,this.initialize()}static get observedAttributes(){this.finalize();const t=[];return this._classProperties.forEach((e,i)=>{const o=this._attributeNameForProperty(i,e);void 0!==o&&(this._attributeToPropertyMap.set(o,i),t.push(o))}),t}static _ensureClassProperties(){if(!this.hasOwnProperty(JSCompiler_renameProperty("_classProperties",this))){this._classProperties=new Map;const t=Object.getPrototypeOf(this)._classProperties;void 0!==t&&t.forEach((t,e)=>this._classProperties.set(e,t))}}static createProperty(t,e=B){if(this._ensureClassProperties(),this._classProperties.set(t,e),e.noAccessor||this.prototype.hasOwnProperty(t))return;const i="symbol"==typeof t?Symbol():"__"+t,o=this.getPropertyDescriptor(t,i,e);void 0!==o&&Object.defineProperty(this.prototype,t,o)}static getPropertyDescriptor(t,e,i){return{get(){return this[e]},set(i){const o=this[t];this[e]=i,this._requestUpdate(t,o)},configurable:!0,enumerable:!0}}static getPropertyOptions(t){return this._classProperties&&this._classProperties.get(t)||B}static finalize(){const t=Object.getPrototypeOf(this);if(t.hasOwnProperty("finalized")||t.finalize(),this.finalized=!0,this._ensureClassProperties(),this._attributeToPropertyMap=new Map,this.hasOwnProperty(JSCompiler_renameProperty("properties",this))){const t=this.properties,e=[...Object.getOwnPropertyNames(t),..."function"==typeof Object.getOwnPropertySymbols?Object.getOwnPropertySymbols(t):[]];for(const i of e)this.createProperty(i,t[i])}}static _attributeNameForProperty(t,e){const i=e.attribute;return!1===i?void 0:"string"==typeof i?i:"string"==typeof t?t.toLowerCase():void 0}static _valueHasChanged(t,e,i=V){return i(t,e)}static _propertyValueFromAttribute(t,e){const i=e.type,o=e.converter||H,n="function"==typeof o?o:o.fromAttribute;return n?n(t,i):t}static _propertyValueToAttribute(t,e){if(void 0===e.reflect)return;const i=e.type,o=e.converter;return(o&&o.toAttribute||H.toAttribute)(t,i)}initialize(){this._saveInstanceProperties(),this._requestUpdate()}_saveInstanceProperties(){this.constructor._classProperties.forEach((t,e)=>{if(this.hasOwnProperty(e)){const t=this[e];delete this[e],this._instanceProperties||(this._instanceProperties=new Map),this._instanceProperties.set(e,t)}})}_applyInstanceProperties(){this._instanceProperties.forEach((t,e)=>this[e]=t),this._instanceProperties=void 0}connectedCallback(){this.enableUpdating()}enableUpdating(){void 0!==this._enableUpdatingResolver&&(this._enableUpdatingResolver(),this._enableUpdatingResolver=void 0)}disconnectedCallback(){}attributeChangedCallback(t,e,i){e!==i&&this._attributeToProperty(t,i)}_propertyToAttribute(t,e,i=B){const o=this.constructor,n=o._attributeNameForProperty(t,i);if(void 0!==n){const t=o._propertyValueToAttribute(e,i);if(void 0===t)return;this._updateState=8|this._updateState,null==t?this.removeAttribute(n):this.setAttribute(n,t),this._updateState=-9&this._updateState}}_attributeToProperty(t,e){if(8&this._updateState)return;const i=this.constructor,o=i._attributeToPropertyMap.get(t);if(void 0!==o){const t=i.getPropertyOptions(o);this._updateState=16|this._updateState,this[o]=i._propertyValueFromAttribute(e,t),this._updateState=-17&this._updateState}}_requestUpdate(t,e){let i=!0;if(void 0!==t){const o=this.constructor,n=o.getPropertyOptions(t);o._valueHasChanged(this[t],e,n.hasChanged)?(this._changedProperties.has(t)||this._changedProperties.set(t,e),!0!==n.reflect||16&this._updateState||(void 0===this._reflectingProperties&&(this._reflectingProperties=new Map),this._reflectingProperties.set(t,n))):i=!1}!this._hasRequestedUpdate&&i&&(this._updatePromise=this._enqueueUpdate())}requestUpdate(t,e){return this._requestUpdate(t,e),this.updateComplete}async _enqueueUpdate(){this._updateState=4|this._updateState;try{await this._updatePromise}catch(t){}const t=this.performUpdate();return null!=t&&await t,!this._hasRequestedUpdate}get _hasRequestedUpdate(){return 4&this._updateState}get hasUpdated(){return 1&this._updateState}performUpdate(){this._instanceProperties&&this._applyInstanceProperties();let t=!1;const e=this._changedProperties;try{t=this.shouldUpdate(e),t?this.update(e):this._markUpdated()}catch(e){throw t=!1,this._markUpdated(),e}t&&(1&this._updateState||(this._updateState=1|this._updateState,this.firstUpdated(e)),this.updated(e))}_markUpdated(){this._changedProperties=new Map,this._updateState=-5&this._updateState}get updateComplete(){return this._getUpdateComplete()}_getUpdateComplete(){return this._updatePromise}shouldUpdate(t){return!0}update(t){void 0!==this._reflectingProperties&&this._reflectingProperties.size>0&&(this._reflectingProperties.forEach((t,e)=>this._propertyToAttribute(e,this[e],t)),this._reflectingProperties=void 0),this._markUpdated()}updated(t){}firstUpdated(t){}}G.finalized=!0;
/**
@license
Copyright (c) 2019 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at
http://polymer.github.io/LICENSE.txt The complete set of authors may be found at
http://polymer.github.io/AUTHORS.txt The complete set of contributors may be
found at http://polymer.github.io/CONTRIBUTORS.txt Code distributed by Google as
part of the polymer project is also subject to an additional IP rights grant
found at http://polymer.github.io/PATENTS.txt
*/
const W="adoptedStyleSheets"in Document.prototype&&"replace"in CSSStyleSheet.prototype,q=Symbol();class J{constructor(t,e){if(e!==q)throw new Error("CSSResult is not constructable. Use `unsafeCSS` or `css` instead.");this.cssText=t}get styleSheet(){return void 0===this._styleSheet&&(W?(this._styleSheet=new CSSStyleSheet,this._styleSheet.replaceSync(this.cssText)):this._styleSheet=null),this._styleSheet}toString(){return this.cssText}}const Y=(t,...e)=>{const i=e.reduce((e,i,o)=>e+(t=>{if(t instanceof J)return t.cssText;if("number"==typeof t)return t;throw new Error(`Value passed to 'css' function must be a 'css' function result: ${t}. Use 'unsafeCSS' to pass non-literal values, but\n            take care to ensure page security.`)})(i)+t[o+1],t[0]);return new J(i,q)};
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */
(window.litElementVersions||(window.litElementVersions=[])).push("2.3.1");const K={};class Q extends G{static getStyles(){return this.styles}static _getUniqueStyles(){if(this.hasOwnProperty(JSCompiler_renameProperty("_styles",this)))return;const t=this.getStyles();if(void 0===t)this._styles=[];else if(Array.isArray(t)){const e=(t,i)=>t.reduceRight((t,i)=>Array.isArray(i)?e(i,t):(t.add(i),t),i),i=e(t,new Set),o=[];i.forEach(t=>o.unshift(t)),this._styles=o}else this._styles=[t]}initialize(){super.initialize(),this.constructor._getUniqueStyles(),this.renderRoot=this.createRenderRoot(),window.ShadowRoot&&this.renderRoot instanceof window.ShadowRoot&&this.adoptStyles()}createRenderRoot(){return this.attachShadow({mode:"open"})}adoptStyles(){const t=this.constructor._styles;0!==t.length&&(void 0===window.ShadyCSS||window.ShadyCSS.nativeShadow?W?this.renderRoot.adoptedStyleSheets=t.map(t=>t.styleSheet):this._needsShimAdoptedStyleSheets=!0:window.ShadyCSS.ScopingShim.prepareAdoptedCssText(t.map(t=>t.cssText),this.localName))}connectedCallback(){super.connectedCallback(),this.hasUpdated&&void 0!==window.ShadyCSS&&window.ShadyCSS.styleElement(this)}update(t){const e=this.render();super.update(t),e!==K&&this.constructor.render(e,this.renderRoot,{scopeName:this.localName,eventContext:this}),this._needsShimAdoptedStyleSheets&&(this._needsShimAdoptedStyleSheets=!1,this.constructor._styles.forEach(t=>{const e=document.createElement("style");e.textContent=t.cssText,this.renderRoot.appendChild(e)}))}render(){return K}}Q.finalized=!0,Q.render=(t,i,o)=>{if(!o||"object"!=typeof o||!o.scopeName)throw new Error("The `scopeName` option is required.");const n=o.scopeName,s=U.has(i),r=N&&11===i.nodeType&&!!i.host,a=r&&!R.has(n),c=a?document.createDocumentFragment():i;if(((t,i,o)=>{let n=U.get(i);void 0===n&&(e(i,i.firstChild),U.set(i,n=new $(Object.assign({templateFactory:_},o))),n.appendInto(i)),n.setValue(t),n.commit()})(t,c,Object.assign({templateFactory:L(n)},o)),a){const t=U.get(c);U.delete(c);const o=t.value instanceof f?t.value.template:void 0;D(n,c,o),e(i,i.firstChild),i.appendChild(c),U.set(i,t)}!s&&r&&window.ShadyCSS.styleElement(i.host)};const X=Y`
:root {
    --coe-teal: #2697B7;
    --coe-blue: #194588;
    --coe-navy: #1c2f3d;
    --coe-orange: #cc4927;
    --coe-sherbet: #edba75;
    --coe-black: black;
    --coe-grey: #535353;
    --coe-light-grey: #e8e8e8;
    --coe-light-gray: #e8e8e8;
    --coe-white: white;
    --coe-blue-hover: #16305d;
    --coe-sherbet-hover: #e09d43;
    --coe-teal-hover: #126379;
    --coe-orange-hover: #b13211;

    /* fonts */
    --coe-font-fam1: museo-sans-display, sans-serif;
    --coe-font-fam2: Lato, sans-serif;
}




body {
    margin: 0;
}

button, .coe-button {
    box-sizing: border-box;
    border-width: 2px;
    border-radius: 24px;
    border-style: solid;
    font-family: var(--coe-font-fam2);
    font-size: 14px;
    line-height: 18px;
    font-weight: 900;
    letter-spacing: 2px;
    padding: 12px 30px;
    text-decoration: none;
}

.coe-black {
    color: var(--coe-black);
}

.coe-blue {
    color: var(--coe-blue);
}

button.style1, a.style1 {
    border-color: var(--coe-blue);
    background-color: var(--coe-blue);
    color: var(--coe-white);
}

button.style1:hover, a.style1:hover {
    background-color: var(--coe-blue-hover);
    border-color: var(--coe-blue-hover);
}

button.style2, a.style2 {
    border-color: var(--coe-blue);
    background-color: var(--coe-white);
    color: var(--coe-blue);
}

button.style2:hover, a.style2:hover {
    background-color: var(--coe-blue);
    border-color: var(--coe-blue);
    color: var(--coe-black);
}

button.style3, a.style3 {
    border-color: var(--coe-sherbet);
    background-color: var(--coe-sherbet);
    color: var(--coe-black);
}

button.style3:hover, a.style3:hover {
    background-color: var(--coe-sherbet-hover);
    border-color: var(--coe-sherbet-hover);
    color: var(--coe-black);
}

button.style4, a.style4 {
    border-color: var(--coe-sherbet);
    background-color: var(--coe-white);
    color: var(--coe-black);
}

button.style4:hover, a.style4:hover {
    background-color: var(--coe-sherbet);
    border-color: var(--coe-sherbet);
    color: var(--coe-black);
}

button.style5, a.style5 {
    border-color: var(--coe-teal);
    background-color: var(--coe-teal);
    color: var(--coe-white);
}

button.style5:hover, a.style5:hover {
    background-color: var(--coe-teal-hover);
    border-color: var(--coe-teal-hover);
    color: var(--coe-white);
}

button.style6, a.style6 {
    border-color: var(--coe-teal);
    background-color: var(--coe-white);
    color: var(--coe-teal);
}

button.style6:hover, a.style6:hover {
    background-color: var(--coe-teal);
    border-color: var(--coe-teal);
    color: var(--coe-white);
}

button.style7, a.style7 {
    border-color: var(--coe-orange);
    background-color: var(--coe-orange);
    color: var(--coe-white);
}

button.style7:hover, a.style7:hover {
    background-color: var(--coe-orange-hover);
    border-color: var(--coe-orange-hover);
    color: var(--coe-white);
}

button.style8, a.style8 {
    border-color: var(--coe-orange);
    background-color: var(--coe-white);
    color: var(--coe-orange);
}

button.style8:hover, a.style8:hover {
    background-color: var(--coe-orange);
    border-color: var(--coe-orange);
    color: var(--coe-white);
}

.coe-grey {
    color: var(--coe-grey);
}

.coe-light-grey {
    color: var(--coe-light-grey);
}

.coe-navy {
    color: var(--coe-navy);
}

.coe-orange {
    color: var(--coe-orange);
}



.coe-sherbet {
    color: var(--coe-sherbet);
}

.coe-teal {
    color: var(--coe-teal);
}

.coe-text118 {
    font-family: var(--coe-font-fam1);
    font-size: 18px;
    line-height: 60px;
    font-weight: 800; 
}

.coe-text135 {
    font-family: var(--coe-font-fam1);
    font-size: 35px;
    line-height: 40px;
    font-weight: 800;
}

.coe-text140, .coe-text-h2, h2 {
    font-family: var(--coe-font-fam1);
    font-size: 40px;
    line-height: 40px;
    font-weight: 800;
}

.coe-text150, .coe-text-h1, h1 {
    font-family: var(--coe-font-fam1);
    font-size: 50px;
    line-height: 60px;
    font-weight: 800;
}

.coe-text118, .coe-text-h5, h5 {
    font-family: var(--coe-font-fam1);
    font-size: 1.125rem;
    font-weight: 900;
}

.coe-text213 {
    font-family: var(--coe-font-fam2);
    font-size: 13px;
    line-height: 24px;
    letter-spacing: 1.5px;
    font-weight: 900;
}

.coe-text215 {
    font-family: var(--coe-font-fam2);
    font-size: 15px;
    line-height: 24px;
    letter-spacing: 1.5px;
    font-weight: 900;
}


.coe-text220, .coe-text-h4, h4 {
    font-family: var(--coe-font-fam2);
    font-size: 20px;
    line-height: 19px;
    letter-spacing: 2.4px;
    font-weight: bold;
}

.coe-text220b {
    font-family: var(--coe-font-fam2);
    font-size: 20px;
    line-height: 24px;
    letter-spacing: 2.4px;
    font-weight: 900;
}

.coe-text222, .coe-text-h3, h3 {
    font-family: var(--coe-font-fam2);
    font-size: 22px;
    line-height: 24px;
    letter-spacing: 2.64px;
    font-weight: 900;
}

.coe-white {
    color: var(--coe-white);
}

div {
    box-sizing: border-box;
}

h3, h4 {
    margin-block-end: 1em;
}


.header-gap {
    margin-bottom: 24px;
}

input[type=text], input[type=email] {
    border: 0;
    background: transparent;
    font-size: 18px;
    font-family: var(--coe-font-fam2);
    padding: 10px;
    border-bottom: 2px solid var(--coe-sherbet);
}



@media only screen and (min-width: 240px) and (max-width: 569px) {

}

@media only screen and (min-width: 570px) and (max-width: 999px) {

}

/* mobile menu sizes */
@media only screen and (max-width: 999px) {
    .page-content {
        padding: 24px 12px;
    }
}

/** desktop menu sizes */
@media only screen and (min-width: 1000px) {
    .page-content {
        padding: 48px 48px 48px 48px;
    }
}

`;window.customElements.define("coe-header-hero",class extends Q{static get styles(){return[X,Y`
      :host {
        display: flex;
        flex-direction: row;
        color: var(--coe-white);
        width: 100%;
      }

      .hero-holder {
          height: 50vw;
          width: 100%;
        }

      .college {
        font-family: 'Gentona';
        font-weight: 400;
        letter-spacing: 0.169em;
        margin-left: 0.863em;
      }

      .college a {
        text-decoration: none;
        color: var(--coe-white);
      }

      div { 
          display: flex;
          flex-direction: row;
      }

      .logo-box {
          background-color: var(--coe-blue);
      }

      .logo-box a {
        height: 100%;
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
      }

      .center-filler {
        flex: 1 1;
      }

      .right-side{
          flex: 1 1;
          justify-content: space-between;
          align-items: center;
          background: linear-gradient(rgba(28, 47, 61, .8), rgba(28, 47, 61, .8), rgba(28, 47, 61, 0));
      }

      .uf-logo {
        fill: var(--coe-white);
      }

      @media only screen and (min-width: 240px) and (max-width: 569px) {
        .college {
          font-size: 4.48vw;
        }

        .logo-box {
          height: 15.8vw;
          width: 15.8vw;
        }

        .right-side {
          height: 15.8vw;
        }

        .uf-logo {
          height: 6.68vw;        
        } 
      }

      @media only screen and (min-width: 570px) and (max-width: 999px) {
        .college {
          font-size: 25.5px;
          margin-left: 22px;
          letter-spacing: 4.3px;
        }

        .logo-box {
          height: 90px;
          width: 90px;
        }

        .right-side {
          height: 90px;
        }

        .uf-logo {
          height: 38px;        
        }   
      }

      @media only screen and (min-width: 1000px) and (max-width: 1400px) {
        .college {
          font-size: 2.55vw;
          margin-left: 2.14vw;
          letter-spacing: 0.43vw;
        }



        .logo-box {
          height: 9.07vw;
          width: 9.07vw;
        }

        .right-side {
          height: 9.07vw;
        }

        .uf-logo {
          height: 3.79vw;        
        }

      }


      @media only screen and (min-width: 1401px) {
        .college {
          font-size: 38px;
          margin-left: 30px;
          letter-spacing: 6px;
        }

        .logo-box {
          height: 127px;
          width: 127px;
        }

        .right-side {
          height: 127px;
        }

        .uf-logo {
          height: 53px;        
        }
      }

    `]}static get properties(){return{image:{type:String},heading:{type:String}}}constructor(){super(),this.image="https://i.coe.ufl.edu/m/norman2.png",this.heading=""}render(){return F`
    <div class="hero-holder" style="background:no-repeat center/100% url('${this.image}');">
      <div class="logo-box">        
          <a href="https://ufl.edu" title="UF">
              <svg class="uf-logo" version="1.1"  xmlns="http://www.w3.org/2000/svg"
                  xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="0 191.7 605.3 408.5" enable-background="new 0 191.7 605.3 408.5" xml:space="preserve" alt="UF Logo">
                      <g>
                          <polygon points="435,527.5 435,424.1 530.3,424.1 530.3,355.9 435,355.9 435,259.4 539.6,259.4 539.6,296.8
                              608.7,296.8 608.7,191.7 332.3,191.7 332.3,259.4 358.2,259.4 358.2,527.5 332.4,527.5 332.4,595.7 460,595.7 460,527.5 	"/>
                          <path d="M296.4,443V259.3h25.6v-67.6H194.1v67.6h24.9v160.9c0,67.4-7.8,99.8-56.2,99.8s-56.2-32.4-56.2-99.8V259.3
                              h24.7v-67.6H3.3v67.6h25.4V443c0,40.1,0,73,18.9,102.5c21.2,33.4,59.5,54.7,115.2,54.7C261.3,600.3,296.4,548.9,296.4,443
                              L296.4,443z"/>
                      </g>
                  </svg>
          </a>
      </div>
      <div class="right-side">
          <div class="college"><a href="/">COLLEGE OF EDUCATION</a></div>
          <div class="center-filler">&nbsp;</div>
          <coe-maga-menu></coe-maga-menu>
      </div>
    </div>
    `}});window.customElements.define("coe-header-solid",class extends Q{static get styles(){return[X,Y`
      :host {
        display: flex;
        flex-direction: row;
        color: var(--coe-white);
        width: 100%;
      }

      .college {
        font-family: 'Gentona';
        font-weight: 400;
        letter-spacing: 0.169em;
        margin-left: 0.863em;
      }

      .college a {
        text-decoration: none;
        color: var(--coe-white);
      }

      div { 
          display: flex;
          flex-direction: row;
      }

      .logo-box {
          background-color: var(--coe-blue);
      }

      .logo-box a {
        height: 100%;
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
      }

      .center-filler {
        flex: 1 1;
      }

      .right-side{
          flex: 1 1;
          justify-content: space-between;
          align-items: center;
          background-color: var(--coe-navy);
      }

      .uf-logo {
        fill: var(--coe-white);
      }

      @media only screen and (min-width: 240px) and (max-width: 569px) {
        .college {
          font-size: 4.48vw;
        }

        .logo-box {
          height: 15.8vw;
          width: 15.8vw;
        }

        .right-side {
          height: 15.8vw;
        }

        .uf-logo {
          height: 6.68vw;        
        } 
      }

      @media only screen and (min-width: 570px) and (max-width: 999px) {
        .college {
          font-size: 25.5px;
          margin-left: 22px;
          letter-spacing: 4.3px;
        }

        .logo-box {
          height: 90px;
          width: 90px;
        }

        .right-side {
          height: 90px;
        }

        .uf-logo {
          height: 38px;        
        }   
      }

      @media only screen and (min-width: 1000px) and (max-width: 1400px) {
        .college {
          font-size: 2.55vw;
          margin-left: 2.14vw;
          letter-spacing: 0.43vw;
        }

        .logo-box {
          height: 9.07vw;
          width: 9.07vw;
        }

        .right-side {
          height: 9.07vw;
        }

        .uf-logo {
          height: 3.79vw;        
        }

      }


      @media only screen and (min-width: 1401px) {
        .college {
          font-size: 38px;
          margin-left: 30px;
          letter-spacing: 6px;
        }

        .logo-box {
          height: 127px;
          width: 127px;
        }

        .right-side {
          height: 127px;
        }

        .uf-logo {
          height: 53px;        
        }
      }

    `]}static get properties(){return{name:{type:String},count:{type:Number}}}constructor(){super(),this.name="Werld",this.count=0}render(){return F`
    <div class="logo-box">        
        <a href="https://ufl.edu" title="UF">
            <svg class="uf-logo" version="1.1"  xmlns="http://www.w3.org/2000/svg"
                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	                viewBox="0 191.7 605.3 408.5" enable-background="new 0 191.7 605.3 408.5" xml:space="preserve" alt="UF Logo">
                    <g>
                        <polygon points="435,527.5 435,424.1 530.3,424.1 530.3,355.9 435,355.9 435,259.4 539.6,259.4 539.6,296.8
                            608.7,296.8 608.7,191.7 332.3,191.7 332.3,259.4 358.2,259.4 358.2,527.5 332.4,527.5 332.4,595.7 460,595.7 460,527.5 	"/>
                        <path d="M296.4,443V259.3h25.6v-67.6H194.1v67.6h24.9v160.9c0,67.4-7.8,99.8-56.2,99.8s-56.2-32.4-56.2-99.8V259.3
                            h24.7v-67.6H3.3v67.6h25.4V443c0,40.1,0,73,18.9,102.5c21.2,33.4,59.5,54.7,115.2,54.7C261.3,600.3,296.4,548.9,296.4,443
                            L296.4,443z"/>
                    </g>
                </svg>
        </a>
    </div>
    <div class="right-side">
        <div class="college"><a href="/">COLLEGE OF EDUCATION</a></div>
        <div class="center-filler">&nbsp;</div>
        <coe-maga-menu></coe-maga-menu>
    </div>
    `}});window.customElements.define("coe-page-type1",class extends Q{static get styles(){return[X,Y`
            :host {
            }`]}static get properties(){return{pageTitle:{type:String},personTitle:{type:String},jobTitle:{type:String},image:{type:String},phone:{type:String},email:{type:String},address:{type:String}}}constructor(){super(),this.pageTitle="Professor McProfessorface",this.personTitle=this.pageTitle,this.jobTitle="Assitant to the Regional Manager",this.image="https://i.coe.ufl.edu/p/sinkhole.jpeg",this.resume="https://i.coe.ufl.edu/d/resumes/404",this.phone="(352) 392-0726",this.email="helpdesk@coe.ufl.edu"}render(){return F`
        <coe-header-solid></coe-header-solid>
        <div class="page-content">
            <div class="coe-text-h2">${this.title}</div>
            <slot></slot>
        </div>
        <coe-footer></coe-footer>
      `}});window.customElements.define("coe-footer",class extends Q{static get styles(){return[X,Y`
      .caep img {
        width: 260px;
      }

      .coe {
        margin-left: 10px;
        font-family: 'Gentona';
        font-size: 18px;
        letter-spacing: 4px;  
      }

      .contact-item {
        margin-bottom: 24px;
      }

      .contact-opt-in {
        display: flex;
        flex-direction: row;
        margin-top: 10px;
      }

      .contact-title {
        margin-top: 1em;
        margin-bottom: 1em;
      }

      .footer{
        display: grid;
      }

      .footer-links {
        background-color: var(--coe-light-grey);
        display: grid;
        padding: 40px;
      }

      .footer-keepup {
        padding: 48px 12px;
        background-color: var(--coe-navy);
        display: flex;
        flex-direction: column;
        align-items: center;
        height: 100%;
        flex: 1;
      }

      .keep-up {
        font-family: var(--coe-font-fam1);
        font-size: 32px;
        line-height: 40px;
        font-weight: 800;
      }

      .logo-box {
        width: 40px;
        height: 40px;
        background-color: var(--coe-blue);
      }

      .logo-box a {
        height: 100%;
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: center;   
      }

      .resources {
        grid-column: 1;
        grid-row: 2;
        margin-bottom: 24px;
      }



      .resources-links {
         display: grid;
      }

      .resources-links a {
        text-decoration: none;
        color: var(--coe-black);
        margin: 10px 22px 10px 0px;
      }

      .right-inner {
        height: 100%;
        display: flex;
        flex: 1;
        flex-direction: column;
        justify-content: space-between;
      }

      .resources-title {
        margin-top: 1em;
        margin-bottom: 1em;
      }

      .social-icons a {
        text-decoration: none;
      }

      .social-icons img {
        margin: 20px 20px 20px 0; 
      }

      .uf-logo {
        height: 16px;
        fill: var(--coe-white);
      }

      .wordmark {
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: left;
        height: 40px;
      }

      /* mobile menu sizes */
      @media only screen and (max-width: 999px) {
        .contact-form {
          display: grid;
        }

        .contact-form .email {
          grid-row: 1;
          max-width: 80vw;
        }

        .contact-form .contact-opt-in {
          grid-row: 2;
        }

        .contact-form .submit {
          grid-row: 3;
          margin-top: 20px;
        }

        .footer-links {
          grid-row: 1;
        }

        .footer-keepup {
          grid-row: 2;
        }

        .resources-links {
          grid-template-columns: 1fr 1fr;
        }

        .social-icons {
          display: flex;
          flex-direction: row;
          justify-content: center;
          margin-top: 20px;
        }
      }

      @media only screen and (min-width: 1000px) and (max-width: 1400px) {
        .caep {
          grid-column: 2;
          grid-row: 3;
        }

        .contact {
          grid-column: 3;
          grid-row: 1 / 3; 
        }


        .footer-links {
          grid-column: 1;
          grid-row: 1;
          grid-template-columns: 1fr 395px 200px 1fr;
        }

        .footer-keepup {
          grid-column: 1;
          grid-row: 2; 
        }


        .resources {
          grid-column: 2;
          grid-row: 2;
        }

        .resources-links {
          grid-template-columns: 1fr 1fr 1fr;
        }

        .wordmark {
          grid-column: 2;
          grid-row: 1;
        }
      }

      @media only screen and (min-width: 1401px) {
        .caep {
          grid-column: 2;
          grid-row: 3;
        }

        .contact {
          grid-column: 3;
          grid-row: 1 / 3; 
        }


        .footer {
          grid-template-columns: 55fr 45fr;
        }

        .footer-links {
          grid-column: 1;
          grid-template-columns: 1fr 395px 230px;
        }

        .footer-keepup {
          grid-column: 2;
          padding: 48px 48px;
        }


        .resources {
          grid-column: 2;
          grid-row: 2;
        }

        .resources-links {
          grid-template-columns: 1fr 1fr 1fr;
        }

        .wordmark {
          grid-column: 2;
          grid-row: 1;
        }
      }

    `]}static get properties(){return{}}constructor(){super()}render(){return F`
      <div class="footer">
        <div class="footer-links">
          <div class=wordmark>
            <div class=logo-box>
              <a href="https://ufl.edu" title="UF">
                <svg class="uf-logo" version="1.1"  xmlns="http://www.w3.org/2000/svg"
                  xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="0 191.7 605.3 408.5" enable-background="new 0 191.7 605.3 408.5" xml:space="preserve" alt="UF Logo">
                    <g>
                        <polygon points="435,527.5 435,424.1 530.3,424.1 530.3,355.9 435,355.9 435,259.4 539.6,259.4 539.6,296.8
                            608.7,296.8 608.7,191.7 332.3,191.7 332.3,259.4 358.2,259.4 358.2,527.5 332.4,527.5 332.4,595.7 460,595.7 460,527.5 	"/>
                        <path d="M296.4,443V259.3h25.6v-67.6H194.1v67.6h24.9v160.9c0,67.4-7.8,99.8-56.2,99.8s-56.2-32.4-56.2-99.8V259.3
                            h24.7v-67.6H3.3v67.6h25.4V443c0,40.1,0,73,18.9,102.5c21.2,33.4,59.5,54.7,115.2,54.7C261.3,600.3,296.4,548.9,296.4,443
                            L296.4,443z"/>
                    </g>
                </svg>
              </a>
            </div>
            <div class=coe>COLLEGE OF EDUCATION</div>
          </div>
          <div class=resources>
            <div class="coe-text220b coe-teal resources-title">
              RESOURCES
            </div>
            <div class=resources-links>
              <a href="/admissions/all-programs">Programs</a>
              <a href="/course-syllabi">Courses</a>
              <a href="/research">Research</a>
              <a href="/alumni">Alumni</a>
              <a href="https://jobs.ufl.edu">Careers</a>
              <a href="/directory">Directory</a>
              <a href="https:///accessibility.ufl.edu">Accessibility</a>
              <a href="mailto:helpdesk@coe.ufl.edu">Webmaster</a>
              <a href="/alumni/impact">Make a Gift</a>
              <a href="/help-and-support">Helpdesk</a>
              <a href="https://www.it.ufl.edu/policies/acceptable-use/acceptable-use-policy/">Acceptable Use</a>
              <a href="https://privacy.ufl.edu/privacy-policies-and-procedures/onlineinternet-privacy-statement/">Privacy Policy</a>
            </div>
          </div>
          <div class=caep>
            <img src="https://i.coe.ufl.edu/home/CAEP-Accredited-Logo-2017-BW@2x.png" />
          </div>
          <div class=contact>
            <div class="coe-text220b coe-teal contact-title">CONTACT</div>
            <div class=contact-item>
              <div class="coe-text215">
                EMAIL
              </div>
              <div class=contact-deet>
                <a href="mailto:helpdesk@ufl.edu">helpdesk@ufl.edu</a>
              </div>
            </div>
            <div class=contact-item>
              <div class="coe-text215">PHONE</div>
              <div class=contact-deet>(352) 392-0726</div>
            </div>
            <div class=contact-item>
              <div class="coe-text215">ADDRESS</div>
              <div class=contact-deet>
                College of Education<br>
                PO Box 117042<br>
                Gainesville, FL 32611-7044<br>
                <a href="http://campusmap.ufl.edu/#/index/0101">View Campus Map</a>
              </div>
            </div>
          </div>
        </div>
        <div class=footer-keepup>
          <div class=right-inner>
            <div class="keep-up coe-white">Keep up with news from UF COE.</div>
            <div class="contact-form">
            <form action="https://ufl.us5.list-manage.com/subscribe/post?u=28a50a3a72926655b31490642&id=9c289bc3f2" method="POST">
              <input class="coe-white email" type="email" value="" placeholder="EMAIL ADDRESS" name="EMAIL" size=40 />
              <input type="hidden" name="b_28a50a3a72926655b31490642_9c289bc3f2" value="">
              <button class="style3 submit">SUBMIT</button>
            </form> 
            </div>
            <div class=social-icons>
              <a href="https://www.facebook.com/UF.COE">
                <img src="https://i.coe.ufl.edu/home/facebook-f-brands.svg">
              </a>
              <a href="https://twitter.com/UF_COE">
                <img src="https://i.coe.ufl.edu/home/twitter-brands.svg">
              </a>
              <a href="https://www.instagram.com/uf_coe/">
                <img src="https://i.coe.ufl.edu/home/instagram-brands.svg">
              </a>
              <a href="https://www.linkedin.com/company/university-of-florida-college-of-education">
                <img src="https://i.coe.ufl.edu/home/linkedin-in-brands.svg">
              </a>
              <a href="https://www.youtube.com/c/UniversityofFloridaCollegeofEducation">
                <img src="https://i.coe.ufl.edu/home/youtube-brands.svg">
              </a>
            </div>
            <div class="copyright coe-white coe-text213">
              &copy; 2021 COLLEGE OF EDUCATION AND UNIVERSITY OF FLORIDA
            </div>
          </div>
        </div>
      </div>
      
    `}});const Z={url:"/",text:"ABOUT"},tt={faculty:[Z,{url:"/faculty",text:"FACULTY"}],home:[Z]};window.customElements.define("coe-breadcrumb",class extends Q{static get styles(){return[X,Y`
        .breadcrumbs {
            font-family: var(--coe-font-fam2);
            font-size: 13px;         
        }

        .breadcrumbs a, .breadcrumbs span {
            text-decoration: none;
            color: var(--coe-orange);
            font-family: var(--coe-font-fam2);
            font-size: 13px;
            line-height: 24px;
            letter-spacing: 1.56px;
            text-transform: capitalize;
            font-weight: 900;
        }

        .breadcrumbs-solid-white {
          background-color: var(--coe-navy);
          padding: 0 12px;
        }

        .breadcrumbs-solid-white a, .breadcrumbs-solid-white .this-page {
          color: var(--coe-white);
        }
        `]}static get properties(){return{path:{type:String},thisPage:{type:String},breadcrumbStyle:{type:String},paths:{attribute:!1}}}constructor(){super(),this.path="home",this.thisPage="set breadcrumb thisPage attribute",this.breadcrumbStyle="default",this.paths=tt}render(){let t="arrow-right.svg";return"solid-white"==this.breadcrumbStyle&&(t="arrow-right-sherbet.svg"),F`
  <div class="breadcrumbs breadcrumbs-${this.breadcrumbStyle}">
    ${this.paths[this.path].map(e=>F`
      <a href="${e.url}">${e.text}</a>
      <img src="https://i.coe.ufl.edu/home/${t}">`)}
    <span class=this-page>${this.thisPage}</span>
  </div>
    `}_onClick(){this.count++}});window.customElements.define("coe-faculty",class extends Q{static get styles(){return[X,Y`
            coe-breadcrumb {
                margin-bottom: 30px;
            }

            .about h4 { 
                margin-top: 0;
            }

            .contact-box {
                background-color: var(--coe-light-grey);
                margin-right: 12px;
                height: fit-content;
            }

            .contact-box a {
                text-decoration: none;
            }

            .contact-box>div {
                padding-left: 12px;
                padding-bottom: 20px;
                padding-right: 12px;
            }

            .contact-twitter img {
                height: 14px; 
            }

            .cv-button {
                display: flex;
                flex-direction: row;
                align-items: center;
            }

            .cv-button-inner {
                margin-bottom: 16px;
                margin-top: 20px;
            }



            .grant {
                max-width: 300px;
                background-color: var(--coe-light-gray);
                padding: 16px;
                border-bottom: 35px solid var(--coe-navy);
            }

            .grant-info-bar {
                width:100%;
                background-color: var(--coe-navy);
                height: 35px;
            }

            .grant-line {
                font-size: 0.8125rem;
                color: var(--coe-navy);
                font-weight: 900;
            }

            .grant-line-detail {
                padding-bottom: .7rem;
                font-weight: normal;
            }

            .grant-line-detail img {
                height: .8rem;
                position: relative;
                top: 2px;
                margin-right: 0.5rem;
            }

            .grants-grid {
                display: grid;
            }

            ul {
                list-style: url("https://i.coe.ufl.edu/entypo/chevron-right-li-orange.svg"); 
            }

            .page-content-container {
                width: 100vw;
                display: flex;
                align-items: center;
                justify-content: center;
            }

            .profile-image {
                width: 100%;
            }

            .publications li {
                margin-bottom: 10px;
            }

            .publications h5 {
                margin-bottom: 10px;
            }

            .title-box h3 {
                margin-top: 0;
            }

            /* cellphone */
            @media only screen and (min-width: 240px) and (max-width: 569px) {
                .grants-grid {
                    gap: 16px;
                    grid-template-columns: 1fr;
                }
            }

            /* large mobile/table */
            @media only screen and (min-width: 570px) and (max-width: 999px) {
                .grants-grid {
                    gap: 16px;
                    grid-template-columns: 1fr 1fr;
                }
            }

            /* mobile menu sizes */
            @media only screen and (max-width: 999px) {
                .about {
                    grid-row: 2;
                }

                .activities {
                    grid-row: 7;
                }

                .affiliations {
                    grid-row: 5;
                }

                .contact-box {
                    grid-row: 3;
                    max-width: 360px;
                }

                .cv-button {
                    grid-row: 4;
                    justify-content: left; 
                }

                .education {
                    grid-row: 8;
                }

                .grants {
                    grid-row: 10;
                }

                .interests {
                    grid-row: 6;
                }

                .news {
                    grid-row: 12;
                }

                .page-content {
                    max-width: 1280px;
                    display: grid; 
                }

                .professional {
                    grid-row: 9;
                }

                .publications {
                    grid-row: 11;
                }

                .title-box {
                    grid-row: 1; 
                }
            }

            /** desktop menu sizes */
            @media only screen and (min-width: 1000px) {
                .about {
                    grid-column: 2 / 6;
                    grid-row: 2;
                }

                .activities {
                    grid-column: 2 / 6;
                    grid-row: 6;
                }

                .affiliations {
                    grid-column: 2 / 4;
                    grid-row: 3;
                }

                .contact-box {
                    grid-column: 1;
                    grid-row: 2 / 9;
                }

                .cv-button {
                    grid-column: 4 / 6;
                    grid-row: 1;
                    justify-content: flex-end;
                }

                .education {
                    grid-column: 2 / 6;
                    grid-row: 4;
                }

                .grants {
                    grid-column: 2 / 6;
                    grid-row: 7;
                }

                .grants-grid {
                    gap: 16px;
                    grid-template-columns: 1fr 1fr;
                }

                .interests {
                    grid-column: 4 / 6;
                    grid-row: 3;
                }

                .news {
                    grid-column: 2 / 6;
                    grid-row: 9;
                }

                .page-content {
                    max-width: 1280px;
                    display: grid; 
                    grid-template-columns: 360px 1fr 1fr 1fr 1fr;
                }

                .professional {
                    grid-column: 2 / 6;
                    grid-row: 5;
                }

                .publications {
                    grid-column: 2 / 6;
                    grid-row: 8;
                }

                .title-box {
                    grid-column: 1/3;
                    grid-row: 1; 
                }
            }

            /* real big */
           @media only screen and (min-width: 1300px) {
            .grants-grid {
                    gap: 16px;
                    grid-template-columns: 1fr 1fr 1fr;
                }
            }
            `]}static get properties(){return{profile:{type:Object}}}constructor(){super();this.profile={name:"Faculty Name",jobTitle:"Job Title Missing",imageUrl:"https://i.coe.ufl.edu/p/sinkhole.jpeg",resumeUrl:"https://i.coe.ufl.edu/d/resumes/404",email:"",website:"",twitter:"",phone:"",address:"",video:"",degrees:"",appointments:"",affiliations:"",interests:"",activities:"",grants:"",publications:"",news:""}}render(){return F`
        <coe-header-solid></coe-header-solid>
        <div class="page-content-container">
            <div class="page-content">
                <div class="title-box">
                    <coe-breadcrumb path="faculty" thisPage="${this.profile.name}"></coe-breadcrumb>
                    <div class="person-title coe-text-h2 coe-teal">${this.profile.name}</div>
                    <h3 class="coe-orange">${this.profile.jobTitle}</h3>
                </div>
                <div class="cv-button">
                    <div class="cv-button-inner">
                        <a class="coe-button style3" href="${this.profile.resumeUrl}">DOWNLOAD CV (PDF)</a>
                    </div>
                </div>
                <div class="contact-box">
                    <img class="profile-image" src=${this.profile.imageUrl} />
                    ${this.profile.phone?F`
                        <div class="contact-phone">
                            <div class="coe-text213 coe-blue">PHONE</div>
                            <div>${this.profile.phone}</div>
                        </div>`:F``}
                    ${this.profile.email?F`
                        <div class="contact-email">
                            <div class="coe-text213 coe-blue">EMAIL</div>
                            <a href="mailto:${this.profile.email}>">${this.profile.email}</a>
                        </div>`:F``}
                    ${this.profile.address?F`
                        <div class="contact-email">
                            <div class="coe-text213 coe-blue">ADDRESS</div>
                            <div>${this.profile.address.map(t=>F`${t}<br>`)}</div>
                        </div>`:F``}
                    ${this.profile.website?F`
                        <div class="contact-email">
                            <div class="coe-text213 coe-blue">WEBSITE</div>
                            <div>${this.profile.website}</div>
                        </div>`:F``}
                    ${this.profile.twitter?F`
                        <div class="contact-twitter">
                            <img src="https://i.coe.ufl.edu/home/twitter-brands-blue.svg">
                            <a href="https://twitter.com/${this.profile.twitter}" target="_blank">@${this.profile.twitter}</a>
                        </div>`:""}
                </div>
                <div class="about">
                    <h4 class="coe-blue">ABOUT</h4>
                    <slot name="about"></slot>
                </div>
                ${this.profile.affiliations?F`
                    <div class="affiliations">
                        <h4 class="coe-blue">AFFILIATIONS</h4>
                        <ul>
                            ${this.profile.affiliations.map(t=>F`<li>${t}</li>`)}
                        </ul>
                    </div>`:F``}
                ${this.profile.interests?F`
                    <div class="interests">
                        <h4 class="coe-blue">RESEARCH INTERESTS</h4>
                        <p>${this.profile.interests.join(", ")}</p>
                    </div>`:F``}
                ${this.profile.degrees?F`
                    <div class="education">
                        <h4 class="coe-blue">EDUCATION</h4>
                        <ul>
                            ${this.profile.degrees.map(t=>F`<li>${t}</li>`)}
                        </ul>
                    </div>`:F``}
                ${this.profile.activities?F`
                    <div class="activities">
                        <h4 class="coe-blue">ACTIVITIES AND HONORS</h4>
                        <ul>
                            ${this.profile.activities.map(t=>F`<li>${t}</li>`)}
                        </ul>
                    </div>`:F``}
                ${this.profile.grants?F`
                    <div class="grants">
                        <h4 class="coe-blue">SELECTED GRANTS</h4>
                        <div class="grants-grid">
                            ${this.profile.grants.map(t=>F`
                                
                                    <div class="grant">
                                        <div class="coe-text-h5">${t.title}</div>
                                        <div class="grant-line">
                                        ROLE
                                            <div class="grant-line-detail">
                                                <img src="https://i.coe.ufl.edu/entypo/chevron-right-li-orange.svg">${t.role}
                                            </div>
                                        </div>
                                        <div class="grant-line">
                                        FUNDING AGENCY
                                            <div class="grant-line-detail">
                                                <img src="https://i.coe.ufl.edu/entypo/chevron-right-li-orange.svg">${t.grantor}
                                            </div>
                                        </div>
                                        <div class="grant-line">
                                        PROJECT PERIOD
                                            <div class="grant-line-detail">
                                                <img src="https://i.coe.ufl.edu/entypo/chevron-right-li-orange.svg">${t.awarddate}
                                            </div>
                                        </div>
                                        <div class="grant-line">
                                        AWARD AMOUNT
                                            <div class="grant-line-detail">
                                                <img src="https://i.coe.ufl.edu/entypo/chevron-right-li-orange.svg">${t.awardamount}
                                            </div>
                                        </div>

                                    </div>

                            `)}
                        </div>
                    </div>`:""}
                ${this.profile.publications?F`
                    <div class="publications">
                        <h4 class="coe-blue">SELECTED PUBLICATIONS</h4>
                        ${Object.entries(this.profile.publications).map(([t,e],i)=>F`
                            <h5 class="publication-category">${t}</h5>
                            <ul>
                                ${e.map(t=>F`
                                    ${"journal"in t?F`
                                    <li>
                                        ${t.authors} (${t.pubdate}). ${t.title}.
                                        <i>${t.journal}</i>
                                        ${t.jnum?F`, ${t.jnum}`:F``} 
                                        ${t.pages?F`, ${t.pages}`:F``}
                                    </li>
                                    `:F`
                                    <li>
                                        ${t.authors} (${t.pubdate}). <i>${t.title}.</i>   
                                        ${t.pubcity}: ${t.publisher}     
                                    </li>`}
                                `)}
                            </ul>
                        `)}
                    </div>`:F``}
                ${this.profile.news?F`
                    <div class="news">
                        <h4 class="coe-blue">RECENT NEWS</h4>
                    </div>`:""} 
            </div>
        </div>
        <coe-footer></coe-footer>
      `}});const et={STUDY:[["Undergraduate","/admissions/undergraduate-programs"],["Graduate","/admissions/graduate-programs"],["Online Learning","/coe-online"]],RESEARCH:[["Office of Educational Research","/educational-research"],["Centers & Institutes","/research/centers-and-institutes"]],CONNECT:[["Alumni","/alumni"],["News","/news"]],ABOUT:[["History","/mission-history"],["Schools","/schools"],["Administration","/administration"],["Faculty","/faculty"],["Impact","/impact"]],CONTACT:[["Make a Gift","/alumni/impact/"],["Directory","/directory"]]};window.customElements.define("coe-maga-menu",class extends Q{static get styles(){return[X,Y`
        .maga-outer {
          display: flex;
          flex-direction: row;
          
        }

        .maga-top {
          position: relative;
          display: flex;
          flex-direction: column;
          justify-content: center;
        }



        #menu-open, #menu-close {
            height: 36px;
            fill: var(--coe-white);
            padding-right: 12px;
        }

        .search-icon {
          fill: var(--coe-white);
        }

        .subitem {
          overflow: visible;
          padding: 10px 0px 10px 0px;
        }



        .submenu a {
          text-decoration: none;
          font-family: var(--coe-font-fam2);
          font-weight: 900;
          font-size: 18px;
          line-height: 24px;
          color: var(--coe-navy);
          white-space: nowrap;
          padding: 10px 24px 10px 24px;
        }



        .top-item {
          font-family: var(--coe-font-fam2);
          font-weight: 900;
        }

        @media only screen and (min-width: 240px) and (max-width: 569px) {

        }

        @media only screen and (min-width: 570px) and (max-width: 999px) {

        }

        /** rules for the mobile menu */
        @media only screen and (max-width: 999px) {
          .maga-outer {
            position: absolute;
            top: 90px;
            display: grid;
            left: 0px;
            color: var(--coe-navy);
            width: 100vw;
            background-color: var(--coe-white);
            padding-bottom: 20px;
          }

          .maga-outer.closed {
            display: none; 
          }

          .maga-top {
            margin: 0 10vw;
            padding: 18px 0;
            font-size: 1.125rem;
            border-bottom: 2px solid var(--coe-sherbet);
          }

          .maga-top.closed .submenu {
            display: none;
          }

          .maga-top.search {
            grid-row: 1;
            justify-content: flex-start;
            align-items: center;
          }

          .maga-top .search-box {
            display: grid;
            grid-template-columns: 1fr 32px;
          }

          .maga-top .top-item-toggle {
            transform: rotate(90deg);
            transition-property: transform;
            transition-duration: 0.5s; 
          }

          .maga-top.closed .top-item-toggle {
            transform: rotate(0deg);
          }

          #menu-button.closed #menu-close {
            display: none;
          }  

          #menu-button #menu-open {
            display: none;
          }

          #menu-button.closed #menu-open {
            display: block;
          }

          .search-icon.top-item {
            display: none;
            grid-column: 2;
            grid-row: 1;
          }

          .search-icon svg {
            height: 24px;
            fill: var(--coe-navy);
          }

          .search-panel {
            grid-column: 1;
            grid-row: 1;
          }

          .search-panel input[type="text"] {
            border: 0;
            background-color: var(--coe-light-gray);
            width: 80%;
          }

          .submenu {
            background-color: var(--coe-light-gray);
            margin-top: 18px;
            padding: 18px 0;
          }

          .top-item {
              display: flex;
              flex-direction: row;
              justify-content: space-between;
          }

          .top-item-toggle img {
            height: 1.125rem;
          }
        }

        /** rules for the big menu */
        @media only screen and (min-width: 1000px) {
          #COET-searchinput {
            margin-right: 12px;
          }

          #COET-searchgo {
            font-family: var(--coe-font-fam2);
          }
          

          .maga-top:hover > .submenu {
            display: block;
          }        
  
          .maga-top:hover > .top-item {
            border-bottom: 2px solid var(--coe-sherbet);
          }

          .search-box {
            display: flex;
            flex-direction: row;
          }

          .search-panel {
            position: absolute;
            background-color: var(--coe-white);
            padding: 10px;
            border: 1px solid rgba(28, 47, 61, 0.3);
            left: -244px;
            display: block;
          }

          .search-panel.closed  {
            display: none;
          }

          .subitem:hover {
            background-color: var(--coe-light-grey);
          }

          .submenu {
            display: none;
            position: absolute;
            background-color: var(--coe-white);
            
            padding-top: 10px;
            padding-bottom: 10px;
            border: 1px solid rgba(28, 47, 61, 0.3);
          }

          .submenu:hover {
            display: block;
          }

          .top-item {
            border-bottom: 2px solid rgba(0,0,0,0);
            border-top: 2px solid var(--coe-sherbet);
          }

          .top-item-toggle {
            display: none;
          }
        } 

        @media only screen and (min-width: 1000px) and (max-width: 1400px) {
          .maga-top {
            height: 9.07vw;
          }

          .menu-button-outer {
            display:none;
          }

          .search-icon svg {
            height: 1.357vw;
          }

          .search-panel {
            top: 6.43vw;
          }

          .submenu {
            top: 7.71vw;
          }

          .top-item {
            font-size: 1.21vw;
            letter-spacing: 0.15vw;
            padding: 1vw 2.23vw 0.71vw 0;
          }

        }

        @media only screen and (min-width: 1401px) {
          .maga-top {
            height: 127px;
          }

          .menu-button-outer {
            display: none;
          }

          .search-icon svg {
            height: 19px;
          }

          .search-panel {
            top: 90px;
          }

          .submenu {
            top: 108px;
          }

          .top-item {
            font-size: 17px;
            letter-spacing: 2.16px;
            padding: 14px 32px 10px 0px;
          }

        }
      `]}static get properties(){return{content:{attribute:!1}}}constructor(){super(),this.content=et}render(){return F`
  <div class="maga-outer closed">
    ${Object.keys(this.content).map(t=>F`
      <div class="maga-top closed" @click="${this._toggleSubmenu}">
        <div class="top-item">
          <div class="top-item-text">${t}</div>
          <div class="top-item-toggle">
            <img src="https://i.coe.ufl.edu/entypo/chevron-right.svg" />
          </div>
        </div>
        <div class="submenu">
          ${this.content[t].map(t=>F`
            <div class="subitem">
              <a href=${t[1]}>${t[0]}</a>
            </div>
          `)}
        </div>
      </div>
    `)}
      <div class="maga-top search">
        <div class="top-item search-icon" @click="${this._toggleSearch}">
          <svg version="1.1" id="Magnifying_glass" xmlns="http://www.w3.org/2000/svg"
                    x="0px" y="0px"
                    viewBox="0 0 20 20" enable-background="new 0 0 20 20"
                    xml:space="preserve">
                  <path d="M17.545,15.467l-3.779-3.779c0.57-0.935,0.898-2.035,0.898-3.21c0-3.417-2.961-6.377-6.378-6.377
                      C4.869,2.1,2.1,4.87,2.1,8.287c0,3.416,2.961,6.377,6.377,6.377c1.137,0,2.2-0.309,3.115-0.844l3.799,3.801
                      c0.372,0.371,0.975,0.371,1.346,0l0.943-0.943C18.051,16.307,17.916,15.838,17.545,15.467z M4.004,8.287
                      c0-2.366,1.917-4.283,4.282-4.283c2.366,0,4.474,2.107,4.474,4.474c0,2.365-1.918,4.283-4.283,4.283
                      C6.111,12.76,4.004,10.652,4.004,8.287z"/>
          </svg>
        </div>
        <div class="search-panel closed"> 
                <form action="https://education.ufl.edu/search-results/" id="cse-search-box" style="">
                  <div class="search-box">
                    <input type="hidden" name="cx" value="002304401973498559738:0320luhr8yc">
                    <input type="hidden" name="cof" value="FORID:9">
                    <input type="hidden" name="ie" value="UTF-8">
                    <input name="siteurl" type="hidden" value="education.ufl.edu/">
                    <input id="COET-searchinput" type="text" name="q" title="Search the College of Education" value="" placeholder="Search">
                    <input type="hidden" name="sa" value="Search">
                    <button class="coe-button style3" id="COET-searchgo">GO</button>
                  </div>

							</form>
            </div>
      </div>
  </div>
  <div class="menu-button-outer">
    <a id="menu-button" class="closed" @click="${this._mmt}">
        <svg version="1.1" id="menu-open" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  viewBox="0 0 20 20" enable-background="new 0 0 20 20" xml:space="preserve">
                <path fill="#FFFFFF" d="M16.4,9H3.6C3.048,9,3,9.447,3,10c0,0.553,0.048,1,0.6,1h12.8c0.552,0,0.6-0.447,0.6-1S16.952,9,16.4,9z
                  M16.4,13H3.6C3.048,13,3,13.447,3,14c0,0.553,0.048,1,0.6,1h12.8c0.552,0,0.6-0.447,0.6-1S16.952,13,16.4,13z M3.6,7h12.8
                    C16.952,7,17,6.553,17,6s-0.048-1-0.6-1H3.6C3.048,5,3,5.447,3,6S3.048,7,3.6,7z"/>
            </svg>
        <svg version="1.1" id="menu-close" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
          viewBox="0 0 20 20" enable-background="new 0 0 20 20" xml:space="preserve">
        <path fill="#FFFFFF" d="M14.348,14.849c-0.469,0.469-1.229,0.469-1.697,0L10,11.819l-2.651,3.029c-0.469,0.469-1.229,0.469-1.697,0
            c-0.469-0.469-0.469-1.229,0-1.697l2.758-3.15L5.651,6.849c-0.469-0.469-0.469-1.228,0-1.697s1.228-0.469,1.697,0L10,8.183
            l2.651-3.031c0.469-0.469,1.228-0.469,1.697,0s0.469,1.229,0,1.697l-2.758,3.152l2.758,3.15
            C14.817,13.62,14.817,14.38,14.348,14.849z"/>
        </svg>
    </a>
  </div>
    `}_mmt(t){this.shadowRoot.querySelector(".maga-outer").classList.toggle("closed"),this.shadowRoot.querySelector("#menu-button").classList.toggle("closed")}_submitSearch(t){this.shadowRoot.querySelector("#cse-search-box").submit()}_toggleSearch(t){this.shadowRoot.querySelector(".search-panel").classList.toggle("closed")}_toggleSubmenu(t){let e=t.composedPath();for(let t in e){let i=e[t];i.classList&&i.classList.contains("maga-top")&&(console.log("maga-top element found"),i.classList.toggle("closed"))}}});window.customElements.define("coe-content-1-1",class extends Q{static get styles(){return[X,Y`
        :host {
            width: 100vw;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
        }
        
        .content-left {
            grid-row: 1;
        }

        .hero {
          width: 100vw;
          display: flex;
          align-items: center;
          justify-content: center;
          flex-direction: column;
        }

        .hero img {
          width: 100%;
          height: 100%;
        }

        .hero-inner {
          display: flex;
          flex-direction: column;
          justify-content: flex-end;
          align-items: flex-start;
          max-width: 1280px;
          height: 100%;
          width: 100%;
          padding: 0 16px;
          margin-bottom: 48px;
          margin-top: 16px;
        }


        .page-content {
            display: grid;
            width: 100%;
            padding: 16px;
            max-width: 1280px;
            background-color: var(--coe-light-gray);
            align-items: center;
        }

        /* cellphone */
        @media only screen and (min-width: 240px) and (max-width: 569px) {
        }


        /* large mobile/table */
        @media only screen and (min-width: 570px) and (max-width: 999px) {
        }

        /* mobile menu sizes */
        @media only screen and (max-width: 999px) {
            .content-left {
                grid-row: 1;
            }

            .content-right {
                grid-row: 2;
            }

            .content-right img {
                width: 100%;
                max-width: 600px;
            }

            .hero {
              min-height: 56vw;
            }
        }

        /** desktop menu sizes */
        @media only screen and (min-width: 1000px) {
            .content-left {
                grid-column: 1;
                padding-right: 24px;
            }

            .content-right {
                grid-column: 2;
            }

            .content-right img {
                width: 100%;
            }

            .hero {
              height: 600px;
            }

            .hero-inner {
              flex-direction: column;  
            } 

            .page-content {
                grid-template-columns: 1fr 1fr;
            }
        }

        /* real big */
        @media only screen and (min-width: 1300px) {
        }
        `]}static get properties(){return{image:{type:String},heading:{type:String},breadcrumbPath:{type:String},thisPage:{type:String}}}constructor(){super(),this.image="https://i.coe.ufl.edu/students-tall.png",this.heading="",this.breadcrumbPath="home",this.thisPage="Here"}render(){return F`
      <div class="hero" style="background:no-repeat center/100% url('${this.image}');">
        <div class="hero-inner">
          <coe-breadcrumb path="${this.breadcrumbPath}" thisPage="${this.thisPage}" breadcrumbStyle="solid-white"></coe-breadcrumb>
          <div class="page-content">
            <div class="content-left">
              <h1 class="coe-orange">${this.heading}</h1>
            </div>
            <div class="content-right">
              <slot></slot>  
            </div>
          </div>
        </div>
      </div>
    `}_onClick(){this.count++}});window.customElements.define("coe-content-2-2",class extends Q{static get styles(){return[X,Y`
        :host {
            width: 100vw;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        
        .content-left {
            grid-row: 1;
        }


        .page-content {
            display: grid;
            width: 100%;
            padding: 16px;
            max-width: 1280px;
        }

        /* cellphone */
        @media only screen and (min-width: 240px) and (max-width: 569px) {
        }


        /* large mobile/table */
        @media only screen and (min-width: 570px) and (max-width: 999px) {
        }

        /* mobile menu sizes */
        @media only screen and (max-width: 999px) {
            .content-left {
                grid-row: 2;
            }

            .content-right {
                grid-row: 1;
            }

            .content-right img {
                width: 100%;
                max-width: 600px;
            }

            .page-content {
                
            }
        }

        /** desktop menu sizes */
        @media only screen and (min-width: 1000px) {
            .content-left {
                grid-column: 1;
                padding-right: 24px;
            }

            .content-right {
                grid-column: 2;
            }

            .content-right img {
                width: 100%;
            }

            h2 {
              margin-top: 0;
            }

            .page-content {
                grid-template-columns: 1fr 1fr;
            }
        }

        /* real big */
        @media only screen and (min-width: 1300px) {
        }
        `]}static get properties(){return{image:{type:String},heading:{type:String},headingColor:{type:String}}}constructor(){super(),this.image="https://i.coe.ufl.edu/students.png",this.heading="",this.headingColor="coe-teal"}render(){return F`
      <div class="page-content">
          <div class="content-left">
            <h2 class="${this.headingColor}">${this.heading}</h2>
            <slot></slot>
          </div>
          <div class="content-right">
            <img src="${this.image}">  
          </div>
      </div>
      
    `}_onClick(){this.count++}});window.customElements.define("coe-content-1-2",class extends Q{static get styles(){return[X,Y`
        :host {
            width: 100vw;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
        }
        
        .content-left {
            grid-row: 1;
        }

        .hero {
          width: 100vw;
          display: flex;
          align-items: center;
          justify-content: center;
          flex-direction: column;
        }

        .hero img {
          width: 100%;
          height: 100%;
        }

        .hero-inner {
          display: flex;
          flex-direction: row;
          justify-content: left;
          align-items: flex-end;
          max-width: 1280px;
          height: 100%;
          width: 100%;
          padding: 0 16px;
        }


        .page-content {
            display: grid;
            width: 100%;
            padding: 16px;
            max-width: 1280px;
        }

        /* cellphone */
        @media only screen and (min-width: 240px) and (max-width: 569px) {
        }


        /* large mobile/table */
        @media only screen and (min-width: 570px) and (max-width: 999px) {
        }

        /* mobile menu sizes */
        @media only screen and (max-width: 999px) {
            .content-left {
                grid-row: 1;
            }

            .content-right {
                grid-row: 2;
            }

            .content-right img {
                width: 100%;
                max-width: 600px;
            }

            .hero {
              height: 56vw;
            }

            .page-content {
                
            }
        }

        /** desktop menu sizes */
        @media only screen and (min-width: 1000px) {
            .content-left {
                grid-column: 1;
                padding-right: 24px;
            }

            .content-right {
                grid-column: 2;
            }

            .content-right img {
                width: 100%;
            }

            .hero {
              height: 400px;
            }

            .page-content {
                grid-template-columns: 1fr 1fr;
            }
        }

        /* real big */
        @media only screen and (min-width: 1300px) {
        }
        `]}static get properties(){return{image:{type:String},heading:{type:String},breadcrumbPath:{type:String},thisPage:{type:String}}}constructor(){super(),this.image="",this.heading="",this.breadcrumbPath="home",this.thisPage="Here"}render(){return F`
      <div class="hero" style="background:no-repeat center/100% url('${this.image}');">
        <div class="hero-inner">
          <coe-breadcrumb path="${this.breadcrumbPath}" thisPage="${this.thisPage}" breadcrumbStyle="solid-white"></coe-breadcrumb>
        </div>
      </div>
      <div class="page-content">
          <div class="content-left">
            <h1 class="coe-orange">${this.heading}</h1>
          </div>
          <div class="content-right">
            <slot></slot>  
          </div>
      </div>
      
    `}_onClick(){this.count++}});window.customElements.define("coe-content-1-6",class extends Q{static get styles(){return[X,Y`
        :host {
            width: 100vw;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        
        .content-left {
            grid-row: 1;
        }

        .content-right {
          background-size: cover;
          background-repeat: no-repeat;
          background-position: center center;
        }

        h1 {
          margin-top: 0;
        }

        .page-content {
            display: grid;
            width: 100%;
            padding: 0 16px;
            max-width: 1280px;
        }

        /* cellphone */
        @media only screen and (min-width: 240px) and (max-width: 569px) {
        }


        /* large mobile/table */
        @media only screen and (min-width: 570px) and (max-width: 999px) {
        }

        /* mobile menu sizes */
        @media only screen and (max-width: 999px) {
            .content-left {
                grid-row: 2;
            }

            .content-right {
                grid-row: 1;
                height: 400px;
            }

            .content-right {
                width: 100%;
                max-width: 600px;
            }

        }

        /** desktop menu sizes */
        @media only screen and (min-width: 1000px) {
            .content-left {
                grid-column: 1;
                padding-right: 24px;
            }

            .content-right {
                grid-column: 2;
            }


            h2 {
              margin-top: 0;
            }

            .page-content {
                grid-template-columns: 70% 30%;
            }
        }

        /* real big */
        @media only screen and (min-width: 1300px) {
        }
        `]}static get properties(){return{image:{type:String},heading:{type:String},subheading:{type:String},headingColor:{type:String},subheadingColor:{type:String},breadcrumbPath:{type:String},thisPage:{type:String}}}constructor(){super(),this.image="https://i.coe.ufl.edu/students-tall.png",this.heading="",this.subheading="",this.headingColor="coe-orange",this.subheadingColor="coe-blue",this.breadcrumbPath="home",this.thisPage="Here"}render(){return F`
      <div class="page-content">
          <div class="content-left">
            <coe-breadcrumb path="${this.breadcrumbPath}" thisPage="${this.thisPage}"></coe-breadcrumb>
            <h1 class="${this.headingColor}">${this.heading}</h1>
            <h3 class="${this.subheadingColor}">${this.subheading}</h3>
            <slot></slot>
          </div>
          <div class="content-right" style="background-image:url('${this.image}');">
            
          </div>
      </div>
      
    `}_onClick(){this.count++}});window.customElements.define("coe-content-2-1",class extends Q{static get styles(){return[X,Y`
        :host {
            width: 100vw;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
        }

        .page-content {
            display: grid;
            width: 100%;
            padding: 16px;
            max-width: 1280px;
        }
        
        /* cellphone */
        @media only screen and (min-width: 240px) and (max-width: 569px) {
        }


        /* large mobile/table */
        @media only screen and (min-width: 570px) and (max-width: 999px) {
        }

        /* mobile menu sizes */
        @media only screen and (max-width: 999px) {
        }

        /** desktop menu sizes */
        @media only screen and (min-width: 1000px) {
        }

        /* real big */
        @media only screen and (min-width: 1300px) {
        }
        
        `]}static get properties(){return{heading:{type:String},headingColor:{type:String}}}constructor(){super(),this.heading="",this.headingColor="coe-teal"}render(){return F`
      <div class="page-content">
        <h2 class="${this.headingColor}">${this.heading}</h2>
        <slot></slot>
      </div>
    `}_onClick(){this.count++}});window.customElements.define("coe-content-3-1",class extends Q{static get styles(){return[X,Y`
        :host {
            width: 100vw;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
        }

        .page-content {
            width: 100%;
            padding: 16px;
            max-width: 1280px;
        }

        .slot-content {
            display: flex;
            justify-content: space-between;
            flex-direction: row;
            align-content: flex-start;
        }

        .slot-content.item-count-2 {
          justify-content: space-around;
        }
        
        /* cellphone */
        @media only screen and (min-width: 240px) and (max-width: 569px) {
        }


        /* large mobile/table */
        @media only screen and (max-width: 640px) {
          .slot-content.item-count-2, .slot-content.item-count-4 {
            flex-direction: column;
            align-items: center;
          }
        }

        /* mobile menu sizes */
        @media only screen and (max-width: 999px) {
          .slot-content.item-count-3 {
            flex-direction: column;
            align-items: center;
          }
        }

        /** desktop menu sizes */
        @media only screen and (min-width: 641px) and (max-width: 1280px) {
          .slot-content.item-count-4 {
            display: grid;
            grid-template-rows: 1fr 1fr;
            grid-template-columns: 300px 300px;
            justify-content: space-around;
          }
        }

        /* real big */
        @media only screen and (min-width: 1300px) {
        }
        
        `]}static get properties(){return{heading:{type:String},headingColor:{type:String},itemCount:{type:String}}}constructor(){super(),this.heading="",this.headingColor="coe-teal",this.itemCount="3"}render(){return F`
      <div class="page-content">
        <h2 class="${this.headingColor}">${this.heading}</h2>
        <div class="slot-content item-count-${this.itemCount}">
          <slot></slot>
        </div>
      </div>
    `}_onClick(){this.count++}});window.customElements.define("coe-content-3-4",class extends Q{static get styles(){return[X,Y`
        :host {
            width: 100vw;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        
        .content-left {
            width: 40vw;
            max-width: 512px;
        }

        .content-right {
          display: flex;
          flex-direction: column;
          justify-content: flex-start;
          align-items: flex-start;
          padding-left: 20px;
          padding-bottom: 20px;
        }

        .page-content {
            display: flex;
            width: 100%;
            padding: 0;
            max-width: 1280px;
        }

        .page-content.background-light {
          background-color: var(--coe-light-gray);
          color: var(--coe-black);
        }

        .page-content.background-dark {
          background-color: var(--coe-navy);
          color: var(--coe-white);
        }

        /* cellphone */
        @media only screen and (min-width: 240px) and (max-width: 569px) {
        }


        /* large mobile/table */
        @media only screen and (min-width: 570px) and (max-width: 999px) {
        }

        /* mobile menu sizes */
        @media only screen and (max-width: 999px) {
            .content-left {
              height: 40vw;
              width: 100vw;
            }

            .page-content {
                flex-direction: column;
            }
        }

        /** desktop menu sizes */
        @media only screen and (min-width: 1000px) {
            .content-left {
                padding-right: 24px;
            }

            h2 {
              margin-top: 0;
            }

            .page-content {
                flex-direction: row;
            }
        }

        /* real big */
        @media only screen and (min-width: 1300px) {
        }
        `]}static get properties(){return{image:{type:String},heading:{type:String},linkUrl:{type:String},linkText:{type:String},contentStyle:{type:String}}}constructor(){super(),this.image="https://i.coe.ufl.edu/students.png",this.heading="",this.linkUrl="#",this.linkText="LANK",this.contentStyle="light"}render(){let t="coe-orange",e="style5",i="background-light";return"dark"==this.contentStyle&&(t="coe-sherbet",e="style7",i="background-dark"),F`
      <div class="page-content ${i}">
          <div class="content-left" style="background:no-repeat center/100% url('${this.image}');">
            
          </div>
          <div class="content-right">
            <h3 class="${t}">${this.heading}</h3>
            <div class="slot">
              <slot></slot>
            </div>
            <a class="coe-button ${e}" href="${this.linkUrl}">${this.linkText}</a>
          </div>
      </div>
      
    `}_onClick(){this.count++}});window.customElements.define("coe-round-blurb",class extends Q{static get styles(){return[X,Y`
        :host {
          width: 300px;
            display: flex;
            align-items: center;
            justify-content: flex-start;
            flex-direction: column;
            padding-bottom: 24px;
        }

        .coe-button {
          margin-top: 20px;
        }

        h3 {
          text-align: center;
        }

        img.round {
          width: 250px;
          height: 250px;
          border-radius: 150px;
        }


        /* cellphone */
        @media only screen and (min-width: 240px) and (max-width: 569px) {
        }


        /* large mobile/table */
        @media only screen and (min-width: 570px) and (max-width: 999px) {
        }

        /* mobile menu sizes */
        @media only screen and (max-width: 999px) {
        }

        /** desktop menu sizes */
        @media only screen and (min-width: 1000px) {
        }

        /* real big */
        @media only screen and (min-width: 1300px) {
        }
        
        `]}static get properties(){return{heading:{type:String},headingColor:{type:String},image:{type:String},linkUrl:{type:String},linkText:{type:String},linkStyle:{type:String}}}constructor(){super(),this.image="https://i.coe.ufl.edu/students-square.png",this.heading="",this.headingColor="coe-orange",this.linkUrl="",this.linkText="",this.linkStyle="style5"}render(){return F`
      <img class="round" src="${this.image}">
      <h3 class="${this.headingColor}">${this.heading}</h3>
      <slot></slot>
      <a class="coe-button ${this.linkStyle}" href="${this.linkUrl}">${this.linkText}</a>
    `}_onClick(){this.count++}});
